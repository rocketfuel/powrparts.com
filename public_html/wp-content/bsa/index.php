<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Select Form</title>
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">
  <style>
    fieldset {
      border: 0;
    }
    label {
      display: block;
      margin: 30px 0 0 0;
    }
    .overflow {
      height: 200px;
    }
  </style>
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
 
  
  
 
  
</head>
<body>
 
<div class="demo">
 
<form action="re.php">
 
  <fieldset>
  <?php require("configure.php");?>
  <?php
$sql = "SELECT * FROM tbl_form ORDER BY form_name";
try {
	$stmt = $DB->prepare($sql);
	$stmt->execute();
	$results = $stmt->fetchAll();
} catch (Exception $ex) {
	echo($ex->getMessage());
}
?>

        <div id="output1">
		<center>
    <label for="Select Option">Select Form</label>
    <select name="form" id="form" onChange="showForm(this);">
	
      
			<option value="">Please Option</option>
		<?php foreach ($results as $rs) { ?>
			<option value="<?php echo $rs["id"]; ?>"><?php echo $rs["form_name"]; ?></option>
		<?php } ?>
	  </select>
 
 
 </fieldset>
</center> 
 
 
 </div> 
  
 
  
</form>
 
</div>
   
 <script>
function showForm(sel) {
	var form_id = sel.options[sel.selectedIndex].value;  
	$("#output1").html( "" );
	if (form_id.length > 0 ) { 
 
	 $.ajax({
			type: "POST",
			url: "checking.php",
			data: "form_id="+form_id,
			cache: false,
			beforeSend: function () { 
				$('#output1').html('<img src="loader.gif" alt="" width="24" height="24">');
			},
			success: function(html) {    
				$("#output1").html( html );
			}
		});
	} 
}
</script>
 
</body>
</html>