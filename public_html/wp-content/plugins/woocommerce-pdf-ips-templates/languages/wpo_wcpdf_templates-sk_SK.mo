��    7      �  I   �      �     �     �     �     �     �     �               )     8     D  6   M     �  C   �  p   �     @     N     S     a  	   m     w     �     �     �     �     �     �     �     �     �     �               8     A     S     W     \     e     n     �     �     �     �     �     �     �  	   �     �     �     �     �     �  ^     �  c  	   5     ?     L     `  	   h     r     �     �  #   �     �     �  3   �       K     �   `     �     �       
     
         +     1     :     K     ]     q     �     �     �     �     �     �     �  	   �                    "     /     7     G     Y     j     x     �     �     �     �     �     �     �     �  
   �  i   �         -   .                     0                 $          
   6             1         *   4   #       !      "              7             &   5   	               3         %      /          +       2                    ,   '                )              (    After discount Before discount Billing Address: Columns Credit Note Credit Note Date: Credit Note Number: Custom Styles Customer Notes Description Discount Drag & drop any of these fields to the documents below Editor Enter any custom styles here to modify/override the template styles Enter the total height of the footer in mm, cm or in and use a dot for decimals.<br/>For example: 1.25in or 82mm Excluding tax Fees Footer height Grand total Include % Including tax Invoice Invoice Date: Invoice Number: Order Date: Order Number: Packing Slip Payment Method: Payment method Price Proforma Invoice Proforma Invoice Date: Proforma Invoice Number: Quantity Reason for refund SKU SKU: Ship To: Shipping Shipping Address: Shipping Method: Shipping method Show SKU Show weight Single price Subtotal Tax rate Thumbnail Total Total price Totals VAT Weight: WooCommerce PDF Invoices & Packing Slips requires %sWooCommerce%s to be installed & activated! Project-Id-Version: WooCommerce PDF Invoices & Packing Slips Premium Templates
POT-Creation-Date: 2017-02-14 14:46+0100
PO-Revision-Date: 2017-02-14 15:05+0100
Language-Team: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.8.11
X-Poedit-Basepath: ..
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;_n:1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;esc_attr__;esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c;_n_noop:1,2;_nx_noop:3c,1,2;__ngettext_noop:1,2
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
Last-Translator: kypo <kypo46@gmail.com>
Language: sk_SK
X-Poedit-SearchPath-0: .
X-Poedit-SearchPathExcluded-0: *.js
 Po zľave Pred zľavou Fakturačná adresa Stĺpce Poznámka Dátum poznámky: Číslo poznámky: Vlastné štýly Poznámka zákazníka k objednávke Popis Zľava Chyť a potiahni akékoľvek pole do dokumentu dolu Editor Vložte ľubovoľné vlastné štýly pre zmenu / prepis štýlov šablóny Zadajte celkovú výšku pätičky v mm, cm alebo v palcoch a použite bodku ako oddelovač desatinných miest.<br/>Napríklad: 1.25in alebo 82mm bez DPH Poplatky Výška pätičky Cena spolu Vrátane % s DPH Faktúra Dátum faktúry: Číslo faktúry: Dátum objednávky: Číslo objednávky: Dodací list Spôsob platby: Spôsob platby Cena Proforma faktúra Dátum proforma faktúry: Číslo proforma faktúry: Množstvo Dôvod vrátenia SKU SKU: Odoslať na: Doprava Dodacia adresa: Spôsob prepravy: Spôsob prepravy Zobraziť SKU Zobraziť hmotnosť Jednotková cena Medzisúčet Sadzba dane Náhľad Celkom Celková cena Spolu DPH Hmotnosť: WooCommerce PDF Invoices & Packing Slips potrebuje nainštalovaný a aktivovaný plugin %sWooCommerce%s ! 