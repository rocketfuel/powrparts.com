<?php

// Exit if accessed directly
if (!defined('ABSPATH'))
    exit;

/**
 * Setting page Class
 * 
 * Handles Settings page functionality of plugin
 * 
 * @package reCAPTCHA for WooCommerce
 * @since 1.0.0
 */
class Woo_Recaptcha_Settings_Tabs {

    function __construct() {
        # code...
    }

    /**
     * Add plugin settings
     * 
     * Handles to add plugin settings in Min/Max Quantites Settings Tab
     * 
     * @package reCAPTCHA for WooCommerce
     * @since 1.0.0
     */
    public function woo_recaptcha_get_settings() {

        $languages = array(
            '' => __('Auto Detect', 'woo_recaptcha'),
            'ar' => __('Arabic', 'woo_recaptcha'),
            'bg' => __('Bulgarian', 'woo_recaptcha'),
            'ca' => __('Catalan', 'woo_recaptcha'),
            'zh-CN' => __('Chinese (Simplified)', 'woo_recaptcha'),
            'zh-TW' => __('Chinese (Traditional)', 'woo_recaptcha'),
            'hr' => __('Croatian', 'woo_recaptcha'),
            'cs' => __('Czech', 'woo_recaptcha'),
            'da' => __('Danish', 'woo_recaptcha'),
            'nl' => __('Dutch', 'woo_recaptcha'),
            'en-GB' => __('English (UK)', 'woo_recaptcha'),
            'en' => __('English (US)', 'woo_recaptcha'),
            'fil' => __('Filipino', 'woo_recaptcha'),
            'fi' => __('Finnish', 'woo_recaptcha'),
            'fr' => __('French', 'woo_recaptcha'),
            'fr-CA' => __('French (Canadian)', 'woo_recaptcha'),
            'de' => __('German', 'woo_recaptcha'),
            'de-AT' => __('German (Austria)', 'woo_recaptcha'),
            'de-CH' => __('German (Switzerland)', 'woo_recaptcha'),
            'el' => __('Greek', 'woo_recaptcha'),
            'iw' => __('Hebrew', 'woo_recaptcha'),
            'hi' => __('Hindi', 'woo_recaptcha'),
            'hu' => __('Hungarain', 'woo_recaptcha'),
            'id' => __('Indonesian', 'woo_recaptcha'),
            'it' => __('Italian', 'woo_recaptcha'),
            'ja' => __('Japanese', 'woo_recaptcha'),
            'ko' => __('Korean', 'woo_recaptcha'),
            'lv' => __('Latvian', 'woo_recaptcha'),
            'lt' => __('Lithuanian', 'woo_recaptcha'),
            'no' => __('Norwegian', 'woo_recaptcha'),
            'fa' => __('Persian', 'woo_recaptcha'),
            'pl' => __('Polish', 'woo_recaptcha'),
            'pt' => __('Portuguese', 'woo_recaptcha'),
            'pt-BR' => __('Portuguese (Brazil)', 'woo_recaptcha'),
            'pt-PT' => __('Portuguese (Portugal)', 'woo_recaptcha'),
            'ro' => __('Romanian', 'woo_recaptcha'),
            'ru' => __('Russian', 'woo_recaptcha'),
            'sr' => __('Serbian', 'woo_recaptcha'),
            'sk' => __('Slovak', 'woo_recaptcha'),
            'sl' => __('Slovenian', 'woo_recaptcha'),
            'es' => __('Spanish', 'woo_recaptcha'),
            'es-419' => __('Spanish (Latin America)', 'woo_recaptcha'),
            'sv' => __('Swedish', 'woo_recaptcha'),
            'th' => __('Thai', 'woo_recaptcha'),
            'tr' => __('Turkish', 'woo_recaptcha'),
            'uk' => __('Ukrainian', 'woo_recaptcha'),
            'vi' => __('Vietnamese', 'woo_recaptcha')
        );

        $languages = apply_filters('woo_recaptcha_add_language', $languages);

        // Global Settings for reCAPTCHA
        $woo_recaptcha_settings = array(
            array(
                'name' => __('Google reCAPTCHA Settings', 'woo_recaptcha'),
                'type' => 'title',
                'desc' => '',
                'id' => 'woo_recaptcha_settings'
            ),
            array(
                'name' => __('Site Key', 'woo_recaptcha'),
                'desc' => sprintf(__('Used for displaying the reCAPTCHA. Grab it %sHere%s.', 'woo_recaptcha'), '<a target="_blank" href="https://www.google.com/recaptcha/admin">', '<a/>'),
                'id' => 'woo_recaptcha_site_key',
                'type' => 'text',
                'class' => 'large-text'
            ),
            array(
                'name' => __('Secret Key', 'woo_recaptcha'),
                'desc' => sprintf(__('Used for communication between your site and Google. Grab it %sHere%s.', 'woo_recaptcha'), '<a target="_blank" href="https://www.google.com/recaptcha/admin">', '<a/>'),
                'id' => 'woo_recaptcha_secret_key',
                'type' => 'text',
                'class' => 'large-text'
            ),
            array(
                'name' => __('Theme', 'woo_recaptcha'),
                'desc' => __('Select reCAPTCHA theme', 'woo_recaptcha'),
                'id' => 'woo_recaptcha_theme',
                'type' => 'select',
                'css' => 'min-width: 250px;',
                'default' => 'light',
                'options' => array(
                    'light' => __('Light', 'woo_recaptcha'),
                    'dark' => __('Dark', 'woo_recaptcha')
                )
            ),
            array(
                'name' => __('Language', 'woo_recaptcha'),
                'desc' => __('Select reCAPTCHA language', 'woo_recaptcha'),
                'id' => 'woo_recaptcha_language',
                'type' => 'select',
                'css' => 'min-width: 250px;',
                'default' => '',
                'options' => $languages
            ),
            array(
                'name' => __('Size', 'woo_recaptcha'),
                'desc' => __('Select reCAPTCHA size', 'woo_recaptcha'),
                'id' => 'woo_recaptcha_size',
                'type' => 'select',
                'css' => 'min-width: 250px;',
                'default' => 'normal',
                'options' => array(
                    'normal' => __('Normal', 'woo_recaptcha'),
                    'compact' => __('Compact', 'woo_recaptcha')
                )
            ),
            array(
                'name' => __('Error Message', 'woo_recaptcha'),
                'desc' => __('Enter the Error Message to display when reCAPTCHA is ignored or it is invalid.', 'woo_recaptcha'),
                'id' => 'woo_recaptcha_error_message',
                'type' => 'text',
                'default' => sprintf(__('Please retry CAPTCHA', 'woo_recaptcha')),
                'class' => 'large-text'
            ),
            array(
                'type' => 'sectionend',
                'id' => 'woo_recaptcha_settings'
            ),
            array(
                'name' => __('Display Settings', 'woo_recaptcha'),
                'type' => 'title',
                'desc' => '',
                'id' => 'woo_recaptcha_display_settings'
            ),
            array(
                'title' => __('Login Form', 'woo_recaptcha'),
                'desc' => __('Check this box to enable reCAPTCHA in WooCommerce login form.', 'woo_recaptcha'),
                'id' => 'woo_recaptcha_login',
                'default' => 'no',
                'type' => 'checkbox'
            ),
            array(
                'title' => __('Registration Form', 'woo_recaptcha'),
                'desc' => __('Check this box to enable reCAPTCHA in WooCommerce registration form.', 'woo_recaptcha'),
                'id' => 'woo_recaptcha_registration',
                'default' => 'no',
                'type' => 'checkbox'
            ),
            array(
                'title' => __('Lost Password Form', 'woo_recaptcha'),
                'desc' => __('Check this box to enable reCAPTCHA in WooCommerce lost password form.', 'woo_recaptcha'),
                'id' => 'woo_recaptcha_lost_password',
                'default' => 'no',
                'type' => 'checkbox'
            ),
            array(
                'title' => __('Checkout Page', 'woo_recaptcha'),
                'desc' => __('Check this box to enable reCAPTCHA in WooCommerce checkout page.', 'woo_recaptcha'),
                'id' => 'woo_recaptcha_checkout',
                'default' => 'no',
                'type' => 'checkbox'
            ),
            array(
                'title' => __('Captcha position', 'woo_recaptcha'),
                'desc' => __('Select reCAPTCHA  position on checkout page', 'woo_recaptcha'),
                'id' => 'woo_recaptcha_checkout_position',
                'type' => 'select',
                'css' => 'min-width: 250px;',
                'default' => 'after_checkout_form',
                'options' => array(
                    'after_checkout_form' => __('After Checkout Form', 'woo_recaptcha'),
                    'before_chekout_form' => __('Before Checkout Form', 'woo_recaptcha'),
                    'checkout_order_review' => __('Checkout Order Review', 'woo_reptcha'),
                    'checkout_after_order_review' => __('After Checkout Order Review', 'woo_reptcha'),
                    'before_place_order' => __('Before Place Order Button', 'woo_reptcha')
                )
            ),
            array(
                'type' => 'sectionend',
                'id' => 'woo_recaptcha_display_settings'
            ),
        );

        return $woo_recaptcha_settings;
    }

    /**
     * Settings Tab
     * 
     * Adds the reCapcha tab to the WooCommerce settings page.
     *
     * @package reCAPTCHA for WooCommerce
     * @since 1.0.0
     */
    public function woo_recaptcha_add_settings_tab($tabs) {

        $tabs['woo_recaptcha'] = __('reCAPTCHA', 'woo_recaptcha');

        return $tabs;
    }

    /**
     * Settings Tab Content
     * 
     * Adds the settings content to the min/max qunatities tab.
     *
     * @package reCAPTCHA for WooCommerce
     * @since 1.0.0
     */
    public function woo_recaptcha_settings_tab_content() {

        woocommerce_admin_fields($this->woo_recaptcha_get_settings());
    }

    /**
     * Update Settings
     * 
     * Updates the settings when being saved.
     *
     *  @package reCAPTCHA for WooCommerce
     * @since 1.0.0
     */
    public function woo_recaptcha_update_settings() {

        woocommerce_update_options($this->woo_recaptcha_get_settings());
    }

    /**
     * Adding Hooks
     * 
     * Adding proper hooks for the shortcodes.
     * 
     * @package reCAPTCHA for WooCommerce
     * @since 1.0.0
     */
    public function add_hooks() {

        // Add filter to addd Min/Max Quantities tab on woocommerce setting page
        add_filter('woocommerce_settings_tabs_array', array($this, 'woo_recaptcha_add_settings_tab'), 99);

        // Add action to add Min/Max Quantities tab content
        add_action('woocommerce_settings_tabs_woo_recaptcha', array($this, 'woo_recaptcha_settings_tab_content'));

        // Add action to save custom update content
        add_action('woocommerce_update_options_woo_recaptcha', array($this, 'woo_recaptcha_update_settings'), 100);
    }

}

?>