jQuery(document).ready(function(){
	// Toggle pickup options pickup
	wf_fedex_load_pickup_options();
	jQuery('#woocommerce_wf_fedex_woocommerce_shipping_pickup_enabled').click(function(){
		wf_fedex_load_pickup_options();
		wf_fedex_load_pickup_address_options();
	});

	// Toggle pickup options pickup address
	wf_fedex_load_pickup_address_options();
	jQuery('#woocommerce_wf_fedex_woocommerce_shipping_use_pickup_address').click(function(){
		wf_fedex_load_pickup_address_options();
	});

	if( jQuery('#woocommerce_wf_fedex_woocommerce_shipping_charges_payment_type') .val() != 'THIRD_PARTY' ){
		jQuery('.thirdparty_grp').closest('tr').hide();
	}
	
	jQuery('#woocommerce_wf_fedex_woocommerce_shipping_charges_payment_type').click(function(){
		me = jQuery(this);
		if( me.val() =='THIRD_PARTY' ){
			jQuery('.thirdparty_grp').closest('tr').show();
		}else{
			jQuery('.thirdparty_grp').closest('tr').hide();
		}
	});


	//myaccount return label
	wf_fedex_return_label_options();
	jQuery('#woocommerce_wf_fedex_woocommerce_shipping_frontend_retun_label').click(function(){
		wf_fedex_return_label_options();
	});



	if( jQuery('#woocommerce_wf_fedex_woocommerce_shipping_customs_duties_payer') .val() != 'THIRD_PARTY' ){
		jQuery('.broker_grp').closest('tr').hide();
	}
	
	jQuery('#woocommerce_wf_fedex_woocommerce_shipping_customs_duties_payer').click(function(){
		me = jQuery(this);
		if( me.val() =='THIRD_PARTY' ){
			jQuery('.broker_grp').closest('tr').show();
		}else{
			jQuery('.broker_grp').closest('tr').hide();
		}
	});

	pack_method	=	jQuery('.packing_method').val();
	if( pack_method != 'box_packing'){
		jQuery('.speciality_box').closest('tr').hide();
	}

	jQuery('.packing_method').change(function(){
		if( pack_method == 'box_packing'){
			jQuery('.speciality_box').closest('tr').show();
		}else{
			jQuery('.speciality_box').closest('tr').hide();
		}
	});
});

function wf_fedex_return_label_options(){
	var checked	=	jQuery('#woocommerce_wf_fedex_woocommerce_shipping_frontend_retun_label').is(":checked");
	if(checked){
		jQuery('.wf_settings_return_label').closest('tr').show();
	}else{
		jQuery('.wf_settings_return_label').closest('tr').hide();
	}
}
function wf_fedex_load_pickup_options(){
	var checked	=	jQuery('#woocommerce_wf_fedex_woocommerce_shipping_pickup_enabled').is(":checked");
	if(checked){
		jQuery('.wf_fedex_pickup_grp').closest('tr').show();
	}else{
		jQuery('.wf_fedex_pickup_grp').closest('tr').hide();
	}
}
function wf_fedex_load_pickup_address_options(){
	var pickup_checked	=	jQuery('#woocommerce_wf_fedex_woocommerce_shipping_use_pickup_address').is(":checked");
	var address_checked	=	jQuery('#woocommerce_wf_fedex_woocommerce_shipping_pickup_enabled').is(":checked");
	if( pickup_checked && address_checked ){
		jQuery('.wf_fedex_pickup_address_grp').closest('tr').show();
	}else{
		jQuery('.wf_fedex_pickup_address_grp').closest('tr').hide();
	}
}
