<?php
/**
 * Woocommerce Settings Tab Class
 * @package     Woocommerce ODFL Edition
 * @author      <https://eniture.com/>
 * @copyright   Copyright (c) 2017, Eniture
 */
if ( ! defined( 'ABSPATH' ) ) {
    exit; 
}

/**
 * Woocommerce Setting Tab Class
 */
    class WC_Settings_ODFL_Freight extends WC_Settings_Page 
    {
        /**
         * Woocommerce Setting Tab Class Constructor
         */
        public function __construct() 
        {
            $this->id = 'odfl_quotes';
            add_filter('woocommerce_settings_tabs_array', array($this, 'add_settings_tab'), 50);
            add_action('woocommerce_sections_' . $this->id, array($this, 'output_sections'));
            add_action('woocommerce_settings_' . $this->id, array($this, 'output'));
            add_action('woocommerce_settings_save_' . $this->id, array($this, 'save'));
        }

        /**
         * ODFL Setting Tab For Woocommerce
         * @param $settings_tabs
         * @return string
         */
        
        public function add_settings_tab( $settings_tabs ) 
        {
            $settings_tabs[$this->id] = __('ODFL Freight', 'woocommerce_odfl_quote');
            return $settings_tabs;
        }
            
        /**
         * ODFL Setting Sections
         * @return string
         */

        public function get_sections() 
        {
            $sections = array(
                ''           => __('Connection Settings', 'woocommerce_odfl_quote'),
                'section-1'  => __('Quote Settings', 'woocommerce_odfl_quote'),
                'section-2'  => __('Warehouses', 'woocommerce_odfl_quote'),
                'section-3'  => __('User Guide', 'woocommerce_odfl_quote'),
            );

            return apply_filters('woocommerce_get_sections_' . $this->id, $sections);
        }
            
        /**
         * ODFL Warehouse Tab
         * @return string
         */

        public function odfl_warehouse()
        {
            include_once( 'template/warehose-template.php' );
            include_once( 'template/dropship-template.php' );
        }
            
        /**
         * ODFL User Guide Tab
         * @return string
        */
            
        public function odfl_user_guide()
        {
            include_once( 'template/guide.php' );
        }

        /**
         * Getting Pages on tab call
         * @param $section
         * @return string/array
         */
        public function get_settings($section = null) 
        {
            switch ($section) 
            {
                case 'section-0' :
                    $settings = ODFL_Connection_Settings::odfl_con_setting();
                    break;
                case 'section-1':
                    $odfl_quote_Settings  = new ODFL_Quote_Settings();
                    $settings             = $odfl_quote_Settings->odfl_quote_settings_tab();
                    break;
                case 'section-2' :
                    $this->odfl_warehouse();
                    $settings = array();
                    break;
                case 'section-3' :
                    $this->odfl_user_guide();
                    $settings = array();
                    break;

                default:
                    $odfl_con_settings  = new ODFL_Connection_Settings();
                    $settings           = $odfl_con_settings->odfl_con_setting();
                    break;
            }
            return apply_filters('woocommerce_odfl_quote', $settings, $section);
        }

        /**
         * Out for wooCommerce setting page
         * @global $current_section
         */
        public function output() 
        {
            global $current_section;
            $settings = $this->get_settings($current_section);
            WC_Admin_Settings::output_fields($settings);
        }
        
        /**
         * ODFL Save Settings
         * @return string
        */
        
        public function save() 
        {
            global $current_section;
            $settings = $this->get_settings($current_section);
            WC_Admin_Settings::save_fields($settings);
        }
    }
    return new WC_Settings_ODFL_Freight();