<?php
/**
 * Creating warehouse database table on plugin activate
 * @package     Woocommerce ODFL Edition
 * @author      <https://eniture.com/>
 * @copyright   Copyright (c) 2017, Eniture
 */
    if ( ! defined( 'ABSPATH' ) ) {
        exit; 
    }

    /**
     * Create warehouse database table
    */

    function create_odfl_wh_db() 
    {
        global $wpdb;
        $warehouse_table  = $wpdb->prefix . "warehouse";
        $origin           = 'CREATE TABLE IF NOT EXISTS ' . $warehouse_table . '(
                            id mediumint(9) NOT NULL AUTO_INCREMENT,
                            city varchar(200) NOT NULL,
                            state varchar(200) NOT NULL,
                            zip varchar(200) NOT NULL,
                            country varchar(200) NOT NULL,
                            location varchar(200) NOT NULL,
                            nickname varchar(200) NOT NULL,
                            PRIMARY KEY  (id) )';
        dbDelta( $origin );
        add_option( 'odfl_db_version', '1.0' );
    }
    
    /**
     * Create LTL Class
     */
    
    function create_odfl_ltl_freight_class()
    {
        if ( ! function_exists( 'create_ltl_class' ) ) 
        {
            wp_insert_term(
                'LTL Freight', 'product_shipping_class', array(
                    'description'  => 'The plugin is triggered to provide an LTL freight quote when the shopping cart contains an item that has a designated shipping class. Shipping class? is a standard WooCommerce parameter not to be confused with freight class? or the NMFC classification system.',
                    'slug'         => 'ltl_freight'
                )
            );
        }
    }
        
    /**
     * Add Option For ODFL
     */ 
    function create_odfl_option()
    {
        $eniture_plugins = get_option( 'EN_Plugins' );

        if( ! $eniture_plugins ) 
            add_option( 'EN_Plugins' , json_encode( array( 'odfl' ) ) );
        else
        {
            $plugins_array = json_decode( $eniture_plugins );

            if( ! in_array( 'odfl' , $plugins_array ) ) 
            {
                array_push( $plugins_array, 'odfl' );
                update_option( 'EN_Plugins' , json_encode( $plugins_array ) );
            }
        }
    }