<?php
/**
 * Get Shipping Carriers Class
 * @package     Woocommerce ODFL Edition
 * @author      <https://eniture.com/>
 * @copyright   Copyright (c) 2017, Eniture
 */
if ( ! defined( 'ABSPATH' ) ) {
    exit; 
}

/**
 * Get ODFL Shipping Quotes Class
 */

    class ODFL_Get_Shipping_Quotes 
    {
        /**
         * Create Shipping Package
         * @param $packages
         * @return array
         */
            
            function odfl_shipping_array( $packages ) 
            {   
                
                $odfl_woo_obj = new ODFL_Woo_Update_Changes();
                
                (strlen(WC()->customer->get_shipping_postcode()) > 0) ? $freight_zipcode = WC()->customer->get_shipping_postcode() : $freight_zipcode = $odfl_woo_obj->odfl_postcode();
                (strlen(WC()->customer->get_shipping_country()) > 0)  ? $freight_country = WC()->customer->get_shipping_country()  : $freight_country = $odfl_woo_obj->odfl_getCountry();

                $lineItem   = array();  
                foreach ($packages['items'] as $item)
                {   
                    $iProductClass = "";
                    if( isset( $item['productClass'] ) && !empty( $item['productClass'] ) )
                    {
                        switch ( $item['productClass'] )
                        {
                            case "92.5": 
                                $iProductClass = 92;
                                break;
                            
                            case "77.5": 
                                $iProductClass = 77;
                                break;
                            
                            default :
                                $iProductClass = $item['productClass'];
                                break;
                        
                        }
                    }
                    
                    $lineItem[] = array(
                        'lineItemHeight'      => $item['productHeight'],
                        'lineItemLength'      => $item['productLength'],
                        'lineItemWidth'       => $item['productWidth'],
                        'lineItemClass'       => $iProductClass,
                        'lineItemWeight'      => $item['productWeight'],
                        'piecesOfLineItem'    => $item['productQty'],
                    );
                }

                $domain  = $_SERVER['SERVER_NAME']; 
                $aPluginVersions = $this->odfl_wc_version_number();
                
                $post_data = array(
                    'plateform'             => 'WordPress',
                    'plugin_version'        => $aPluginVersions["odfl_plugin_version"],
                    'wordpress_version'     => get_bloginfo('version'),
                    'woocommerce_version'   => $aPluginVersions["woocommerce_plugin_version"],
                    'licence_key'          => get_option( 'wc_settings_odfl_plugin_licence_key' ),
                    'sever_name'           => $this->odfl_parse_url($domain),
                    'carrierName'          => 'odfl4me', 
                    'carrier_mode'         => 'pro', 
                    'odflUserName'         => get_option( 'wc_settings_odfl_username' ),
                    'odflPassword'         => get_option( 'wc_settings_odfl_password' ),
                    'odflCustomerAccount'  => get_option( 'wc_settings_odfl_account_no' ),
                    'senderZip'            => $packages['origin']['zip'],
                    'senderCountryCode'    => $this->odfl_get_country_code($packages['origin']['country']),
                    'receiverZip'          => $freight_zipcode,
                    'receiverCountryCode'  => $this->odfl_get_country_code($freight_country),
                    'shipType'             => 'LTL',
                    'accessorial'          => array(
                        ( get_option( 'odfl_liftgate' ) == 'yes' )    ? 'HYD' : '',
                        ( get_option( 'odfl_residential' ) == 'yes' ) ? 'RDC' : '',
                        ( $packages['hazardousMaterial'] == 'yes' )   ? 'HAZ' : ''
                    ),
                    'commdityDetails'  => array(
                        'handlingUnitDetails' => $lineItem,
                    ) 
                );

                return $post_data;
            }
            
        /**
         * Get ODFL Country Code
         * @param $sCountryName
         * @return string
         */    
            
            function odfl_get_country_code( $sCountryName )
            {
                switch ( trim( $sCountryName ) ) 
                {
                    case 'CN':
                              $sCountryName = "CAN";
                              break;
                    case 'CA':
                              $sCountryName = "CAN";
                              break;      
                    case 'CAN':
                              $sCountryName = "CAN";
                              break;
                    case 'US':
                              $sCountryName = "USA";
                              break;      
                    case 'USA':
                              $sCountryName = "USA";
                              break;              
                }
                return $sCountryName;
            }

            /**
             * Get Nearest Address If Multiple Warehouses
             * @param $warehous_list
             * @param $receiverZipCode
             * @return array
             */            
            function odfl_multi_warehouse( $warehous_list, $receiverZipCode ) 
            {
                if ( count( $warehous_list ) == 1 ) 
                {
                    $warehous_list = reset( $warehous_list );
                    return $this->odfl_origin_array( $warehous_list );
                }

                foreach ( $warehous_list as $key => $value ) 
                {
                    $origin_zip             = $value->zip;
                    $map_address            = array('originZipCode' => preg_replace( '/\s+/', '', $origin_zip ), 'destinationZipCode' => preg_replace( '/\s+/', '', $receiverZipCode ));
                    $accessLevel            = 'distance';
                    $odfl_distance_request  = new ODFL_Get_Distance();
                    $response_json          = $odfl_distance_request->odfl_address($map_address, $accessLevel);
                    $json_decode            = json_decode($response_json);
                    
                    if( isset( $json_decode->origin_addresses  ) ) 
                    {
                        $dis                                   = array('address' => $json_decode->origin_addresses[0], 'distance' => ( isset($json_decode->rows[0]->elements[0]->distance) ) ? $json_decode->rows[0]->elements[0]->distance : '' );
                        $distance                              = $dis['distance'];
                        $distance_array['distance_km'][]       = ( isset($distance->text) )  ? $distance->text  : '';
                        $distance_array['distance_m'][]        = ( isset($distance->value) ) ? $distance->value : '';
                        $distance_array['origin'][]            = $dis['address'];
                        $distance_array['origin']['location']  = $value->location;
                        $distance_array['id'][]                = $value->id;
                    }
                }
                
                if( isset( $distance_array ) ) 
                {
                    $array                       = array_keys($distance_array['distance_m'], min($distance_array['distance_m']));
                    $n                           = $array[0];
                    $origin                      = explode(",", $distance_array['origin'][$n]);
                    $zip                         = explode(" ", trim($origin[1]));
                    $origin_address              = array();
                    $origin_address['city']      = trim($origin[0]);
                    $origin_address['state']     = $zip[0];
                    $origin_address['zip']       = $zip[1];
                    $origin_address['country']   = $origin[2];
                    $origin_address['location']  = $distance_array['origin']['location'];
                    $origin_address['id']        = $distance_array['id'][$n];
                    return $this->odfl_origin_array((object)$origin_address);
                } 
            }
        
            /**
             * Create Origin Array
             * @param $origin
             * @return string
             */
            function odfl_origin_array( $origin ) 
            {
                return array('locationId' => $origin->id, 'zip' => $origin->zip, 'city' => $origin->city, 'state' => $origin->state,  'location' => $origin->location, 'country' => $origin->country);
            }

          /** 
           * Refine URL
           * @param $domain
           * @return string
           */  
            function odfl_parse_url( $domain ) 
            {
                $domain  = trim( $domain );
                $parsed  = parse_url( $domain );
                
                if ( empty( $parsed['scheme'] ) ){ 
                    $domain = 'http://' . ltrim( $domain, '/' );
                }
                
                $parse                 = parse_url( $domain );
                $refinded_domain_name  = $parse['host'];
                $domain_array          = explode( '.', $refinded_domain_name );
                
                if ( in_array( 'www', $domain_array ) ) 
                {
                    $key = array_search( 'www', $domain_array );
                    unset( $domain_array[$key] );
                    $refinded_domain_name = implode( $domain_array, '.' );
                }
                return $refinded_domain_name;
            }
            
            /**
             * Curl Request To Get Quotes
             * @param $request_data
             * @return json
             */
            function odfl_get_web_quotes($request_data) 
            {  
                if ( is_array($request_data) && count($request_data) > 0 ) 
                {
                    $odfl_curl_obj = new ODFL_Curl_Request();
                    $output        = $odfl_curl_obj->odfl_get_curl_response('http://eniture.com/ws/index.php', $request_data);
                    return $this->parse_odfl_output($output);
                }
            }

            /**
             * Get Shipping Array For Single Shipment
             * @param $output
             * @return string
             */
                
            function parse_odfl_output( $output ) 
            {               
                $result = json_decode( $output );
                
                if( isset( $result->soapenvBody->ns2getLTLRateEstimateResponse->return ) && $result->soapenvBody->ns2getLTLRateEstimateResponse->return->success == true  && empty( $result->soapenvBody->ns2getLTLRateEstimateResponse->return->errorMessages ) && !empty( $result->soapenvBody->ns2getLTLRateEstimateResponse->return->rateEstimate->netFreightCharge ) ) 
                {   
                    $quotes = array(
                        'cost'          => $result->soapenvBody->ns2getLTLRateEstimateResponse->return->rateEstimate->netFreightCharge,
                        'transit_time'  => $result->soapenvBody->ns2getLTLRateEstimateResponse->return->destinationCities->serviceDays
                    );               
                }
                else
                {
                    $quotes = "error";
                } 
                return $quotes;
            }
            
            /**
            * Return woocomerce and abf version
            * @return int
            */
           function odfl_wc_version_number() 
           {
               if (!function_exists('get_plugins'))
               require_once( ABSPATH . 'wp-admin/includes/plugin.php' );

               $plugin_folder       = get_plugins('/' . 'woocommerce');
               $plugin_file         = 'woocommerce.php';
               $odfl_plugin_folder = get_plugins('/' . 'ltl-freight-quotes-odfl-edition');
               $odfl_plugin_file   = 'ltl-freight-quotes-odfl-edition.php';
               $wc_plugin           = (isset($plugin_folder[$plugin_file]['Version']))              ? $plugin_folder[$plugin_file]['Version']             : "";
               $odfl_plugin        = (isset($odfl_plugin_folder[$odfl_plugin_file]['Version']))  ? $odfl_plugin_folder[$odfl_plugin_file]['Version'] : "";

               $pluginVersions = array(
                   "woocommerce_plugin_version"    => $wc_plugin,
                   "odfl_plugin_version"  => $odfl_plugin
               );

               return $pluginVersions;
           }
    }
