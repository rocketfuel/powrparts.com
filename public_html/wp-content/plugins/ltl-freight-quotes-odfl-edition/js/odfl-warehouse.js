// ==================================//
// Warehouse Section Script Start
// ==================================//

    function odfl_save_warehouse()
    {
        var city         = jQuery('#odfl_origin_city').val();
        var zip_err      = false;
        var city_err     = false;
        var state_err    = false;
        var country_err  = false;
        
        if ( jQuery('#odfl_origin_zip' ).val() === '')
        {
            jQuery( '.zip_invalid' ).hide();
            jQuery( '.odfl_zip_validation_err' ).remove();
            jQuery( '#odfl_origin_zip' ).after('<span class="odfl_zip_validation_err">Zip is required.</span>');
            zip_err = 1; 
        }

        if ( city === '')
        {
            jQuery( '.odfl_city_validation_err' ).remove();
            jQuery( '#odfl_origin_city' ).after('<span class="odfl_city_validation_err">City is required.</span>');
            city_err = 1;
        }

        if ( jQuery('#odfl_origin_state' ).val() === '')
        {
            jQuery( '.odfl_state_validation_err' ).remove();
            jQuery( '#odfl_origin_state' ).after('<span class="odfl_state_validation_err">State is required.</span>');
            state_err = 1;
        }

        if ( jQuery('#odfl_origin_country' ).val() === '')
        {
            jQuery( '.odfl_country_validation_err' ).remove();
            jQuery( '#odfl_origin_country' ).after('<span class="odfl_country_validation_err">Country is required.</span>');
            country_err = 1;
        }

        if (zip_err || city_err || state_err || country_err){ 
            return false;
        }

        var postForm  = {
            'action'          : 'odfl_save_warehouse',
            'origin_id'       : jQuery('#edit_form_id').val(),
            'origin_city'     : city,
            'origin_state'    : jQuery('#odfl_origin_state').val(),
            'origin_zip'      : jQuery('#odfl_origin_zip').val(),
            'origin_country'  : jQuery('#odfl_origin_country').val(),
            'location'        : jQuery('#odfl_location').val()
        };

        jQuery.ajax({
            type      : 'POST', 
            url       : ajaxurl, 
            data      : postForm, 
            dataType  : 'json',

            success: function (data) 
            {
                var WarehpuseDataId = data.id;
                if (data.insert_qry == 1) 
                {
                    jQuery( '.warehouse_created' ).show( 'slow' ).delay(5000).hide('slow');
                    jQuery( '.warehouse_updated' ).css( 'display' , 'none' );
                    jQuery( '.warehouse_deleted' ).css( 'display' , 'none' );
                    jQuery( '.dropship_deleted' ).css( 'display' , 'none' );
                    jQuery( '.dropship_updated' ).css( 'display' , 'none' );
                    jQuery( '.dropship_created' ).css( 'display' , 'none' );
                    window.location.href = jQuery( '.close' ).attr( 'href' );
                    jQuery( '#append_warehouse tr:last' ).after( '<tr class="new_warehouse_add" id="row_'+WarehpuseDataId+'" data-id="'+WarehpuseDataId+'"><td class="odfl_warehouse_list_data">'+data.origin_city+'</td><td class="odfl_warehouse_list_data">'+data.origin_state+'</td><td class="odfl_warehouse_list_data">'+data.origin_zip+'</td><td class="odfl_warehouse_list_data">'+data.origin_country+'</td><td class="odfl_warehouse_list_data"><a href="javascript(0)" onclick="return odfl_edit_warehouse('+WarehpuseDataId+')"><img src="'+script.pluginsUrl+'/ltl-freight-quotes-odfl-edition/asset/edit.png" title="Edit" ></a><a href="javascript(0)" onclick="return odfl_delete_current_warehouse('+ WarehpuseDataId +');"><img src="'+script.pluginsUrl+'/ltl-freight-quotes-odfl-edition/asset/delete.png" title="Delete" ></a></td></tr>' );
                }
                else if(data.update_qry == 1)
                {
                    jQuery( '.warehouse_updated' ).show( 'slow' ).delay(5000).hide('slow');
                    jQuery( '.warehouse_created' ).css( 'display' , 'none' );
                    jQuery( '.warehouse_deleted' ).css( 'display' , 'none' );
                    jQuery( '.dropship_deleted' ).css( 'display' , 'none' );
                    jQuery( '.dropship_updated' ).css( 'display' , 'none' );
                    jQuery( '.dropship_created' ).css( 'display' , 'none' );
                    window.location.href = jQuery( '.close' ).attr( 'href' );
                    jQuery( 'tr[id=row_'+WarehpuseDataId+']' ).html( '<td class="odfl_warehouse_list_data">'+data.origin_city+'</td><td class="odfl_warehouse_list_data">'+data.origin_state+'</td><td class="odfl_warehouse_list_data">'+data.origin_zip+'</td><td class="odfl_warehouse_list_data">'+data.origin_country+'</td><td class="odfl_warehouse_list_data"><a href="javascript(0)" onclick="return odfl_edit_warehouse('+WarehpuseDataId+')"><img src="'+script.pluginsUrl+'/ltl-freight-quotes-odfl-edition/asset/edit.png" title="Edit" ></a><a href="javascript(0)" onclick="return odfl_delete_current_warehouse('+ WarehpuseDataId +');"><img src="'+script.pluginsUrl+'/ltl-freight-quotes-odfl-edition/asset/delete.png" title="Delete" ></a></td>' );
                }
                else if( data == false ) 
                {
                    jQuery( '.odfl_warehouse_invalid_input_message' ).show( 'slow' );
                    setTimeout( function () {
                        jQuery( '.odfl_warehouse_invalid_input_message' ).hide( 'slow' );
                    }, 5000);
                }
                else
                {
                    jQuery('.already_exist').show('slow');
                    setTimeout(function () {
                        jQuery('.already_exist').hide('slow');
                    }, 5000);
                }
            }
        }); 
        return false;
    }


    function odfl_delete_current_warehouse(e)
    {
        var postForm = {
            'action'      : 'odfl_delete_warehouse',
            'delete_id'   : e
        };
        
        jQuery.ajax({
            type      : 'POST', 
            url       : ajaxurl, 
            data      : postForm, 
            dataType  : 'json',

            success: function (data) 
            {
                if (data == 1) 
                {
                    jQuery('#row_'+e).remove();
                    jQuery( '.warehouse_deleted' ).show( 'slow' ).delay(5000).hide('slow');
                    jQuery( '.warehouse_updated' ).css( 'display' , 'none' );
                    jQuery( '.warehouse_created' ).css( 'display' , 'none' );
                    jQuery( '.dropship_deleted' ).css( 'display' , 'none' );
                    jQuery( '.dropship_updated' ).css( 'display' , 'none' );
                    jQuery( '.dropship_created' ).css( 'display' , 'none' );
                }
            }
        }); 
        return false;
    }

    function odfl_edit_warehouse(e)
    {   
        jQuery( '.odfl_zip_validation_err' ).hide();
        jQuery( '.odfl_city_validation_err' ).hide();
        jQuery( '.odfl_state_validation_err' ).hide();
        jQuery( '.odfl_country_validation_err' ).hide();
                  
        var postForm  = {
            'action'   : 'odfl_edit_warehouse',
            'edit_id'  : e
        };
                    
        jQuery.ajax({
            type      : 'POST', 
            url       : ajaxurl, 
            data      : postForm, 
            dataType  : 'json',

            success: function (data) 
            {   
                if (data[0]) 
                {
                    jQuery( '#edit_form_id' ).val( data[0].id );
                    jQuery( '#odfl_origin_zip' ).val( data[0].zip );
                    jQuery( '.city_select' ).hide();
                    jQuery( '.city_input' ).show();
                    jQuery( '#odfl_origin_city' ).val( data[0].city );
                    jQuery( '#odfl_origin_city' ).css('background', 'none');
                    jQuery( '#odfl_origin_state' ).val( data[0].state );
                    jQuery( '#odfl_origin_country' ).val( data[0].country );
                    window.location.href = jQuery('.odfl_add_warehouse_btn').attr('href');
                    jQuery( '.already_exist' ).hide();
                    setTimeout(function(){
                        if(jQuery('.odfl_add_warehouse_popup').is(':visible')){
                            jQuery('.odfl_add_warehouse_input > input').eq(0).focus();
                        }
                    },500);
                }
            }
        }); 
        return false;
    }

// ==================================//
// Dropship Section Script Start
// ==================================//

    function save_odfl_dropship()
    { 
        var city         = jQuery('#odfl_dropship_city').val();
        var zip_err      = false;
        var city_err     = false;
        var state_err    = false;
        var country_err  = false;
        
        if ( jQuery('#odfl_dropship_zip' ).val() === '')
        {
            jQuery( '.zip_invalid' ).hide();
            jQuery( '.odfl_zip_validation_err' ).remove();
            jQuery( '#odfl_dropship_zip' ).after('<span class="odfl_zip_validation_err">Zip is required.</span>');
            zip_err = 1; 
        }

        if ( city === '')
        {
            jQuery( '.odfl_city_validation_err' ).remove();
            jQuery( '#odfl_dropship_city' ).after('<span class="odfl_city_validation_err">City is required.</span>');
            city_err = 1;
        }

        if ( jQuery('#odfl_dropship_state' ).val() === '')
        {
            jQuery( '.odfl_state_validation_err' ).remove();
            jQuery( '#odfl_dropship_state' ).after('<span class="odfl_state_validation_err">State is required.</span>');
            state_err = 1;
        }

        if ( jQuery('#odfl_dropship_country' ).val() === '')
        {
            jQuery( '.odfl_country_validation_err' ).remove();
            jQuery( '#odfl_dropship_country' ).after('<span class="odfl_country_validation_err">Country is required.</span>');
            country_err = 1;
        }

        if (zip_err || city_err || state_err || country_err) {
            return false;
        }

        var postForm = {
            'action'            : 'odfl_save_dropship',
            'dropship_id'       : jQuery( '#edit_dropship_form_id' ).val(),
            'dropship_city'     : city,
            'dropship_state'    : jQuery( '#odfl_dropship_state' ).val(),
            'dropship_zip'      : jQuery( '#odfl_dropship_zip' ).val(),
            'dropship_country'  : jQuery( '#odfl_dropship_country' ).val(),
            'nickname'          : jQuery( '#odfl_dropship_nickname' ).val(),
            'location'          : jQuery( '#odfl_dropship_location' ).val()
        };

        jQuery.ajax({
            type      : 'POST', 
            url       : ajaxurl, 
            data      : postForm, 
            dataType  : 'json',

            success: function (data) 
            {
                var WarehpuseDataId = data.id;
                if (data.insert_qry == 1) 
                {
                    jQuery( '.dropship_created' ).show( 'slow' ).delay(5000).hide('slow');
                    jQuery( '.dropship_updated' ).css( 'display' , 'none' );
                    jQuery( '.dropship_deleted' ).css( 'display' , 'none' );
                    jQuery( '.warehouse_deleted' ).css( 'display' , 'none' );
                    jQuery( '.warehouse_updated' ).css( 'display' , 'none' );
                    jQuery( '.warehouse_created' ).css( 'display' , 'none' );
                    window.location.href = jQuery( '.close' ).attr( 'href' );
                    jQuery( '#append_dropship tr:last' ).after( '<tr class="new_dropship_add" id="row_'+WarehpuseDataId+'" data-id="'+WarehpuseDataId+'"><td class="odfl_dropship_list_data">'+data.nickname+'</td><td class="odfl_dropship_list_data">'+data.origin_city+'</td><td class="odfl_dropship_list_data">'+data.origin_state+'</td><td class="odfl_dropship_list_data">'+data.origin_zip+'</td><td class="odfl_dropship_list_data">'+data.origin_country+'</td><td class="odfl_dropship_list_data"><a href="javascript(0)" onclick="return odfl_edit_dropship('+WarehpuseDataId+')"><img src="'+script.pluginsUrl+'/ltl-freight-quotes-odfl-edition/asset/edit.png" title="Edit" ></a><a href="javascript(0)" onclick="return odfl_delete_current_dropship('+ WarehpuseDataId +');"><img src="'+script.pluginsUrl+'/ltl-freight-quotes-odfl-edition/asset/delete.png" title="Delete" ></a></td></tr>' );
                }
                else if(data.update_qry == 1)
                {
                    jQuery( '.dropship_updated' ).show( 'slow' ).delay(5000).hide('slow');
                    jQuery( '.dropship_created' ).css( 'display' , 'none' );
                    jQuery( '.dropship_deleted' ).css( 'display' , 'none' );
                    jQuery( '.warehouse_deleted' ).css( 'display' , 'none' );
                    jQuery( '.warehouse_updated' ).css( 'display' , 'none' );
                    jQuery( '.warehouse_created' ).css( 'display' , 'none' );
                    window.location.href = jQuery( '.close' ).attr( 'href' );
                    jQuery( 'tr[id=row_'+WarehpuseDataId+']' ).html( '<td class="odfl_dropship_list_data">'+data.nickname+'</td><td class="odfl_dropship_list_data">'+data.origin_city+'</td><td class="odfl_dropship_list_data">'+data.origin_state+'</td><td class="odfl_dropship_list_data">'+data.origin_zip+'</td><td class="odfl_dropship_list_data">'+data.origin_country+'</td><td class="odfl_dropship_list_data"><a href="javascript(0)" onclick="return odfl_edit_dropship('+WarehpuseDataId+')"><img src="'+script.pluginsUrl+'/ltl-freight-quotes-odfl-edition/asset/edit.png" title="Edit" ></a><a href="javascript(0)" onclick="return odfl_delete_current_dropship('+ WarehpuseDataId +');"><img src="'+script.pluginsUrl+'/ltl-freight-quotes-odfl-edition/asset/delete.png" title="Delete" ></a></td>' );
                }
                else if( data == false ) 
                {
                    jQuery( '.odfl_dropship_invalid_input_message' ).show( 'slow' );
                    setTimeout( function () {
                        jQuery( '.odfl_dropship_invalid_input_message' ).hide( 'slow' );
                    }, 5000);
                }
                else
                {
                    jQuery('.already_exist').show('slow');
                    setTimeout(function () {
                        jQuery('.already_exist').hide('slow');
                    }, 5000);
                }
            }
        }); 
        return false;
    }


    function odfl_edit_dropship(e)
    {
        jQuery('.odfl_zip_validation_err').hide();
        jQuery('.odfl_city_validation_err').hide();
        jQuery('.odfl_state_validation_err').hide();
        jQuery('.odfl_country_validation_err').hide();
        
        var postForm = {
            'action'            : 'odfl_edit_dropship',
            'dropship_edit_id'  : e
        };
                    
        jQuery.ajax({
            type      : 'POST', 
            url       : ajaxurl, 
            data      : postForm, 
            dataType  : 'json',

            success: function (data) 
            {
                if (data[0]) 
                {
                    jQuery( '#edit_dropship_form_id' ).val( data[0].id );
                    jQuery( '#odfl_dropship_zip' ).val( data[0].zip );
                    jQuery( '#odfl_dropship_nickname' ).val( data[0].nickname );
                    jQuery( '.city_select' ).hide();
                    jQuery( '.city_input' ).show();
                    jQuery( '#odfl_dropship_city' ).val( data[0].city );
                    jQuery( '#odfl_dropship_city' ).css('background', 'none'); 
                    jQuery( '#odfl_dropship_state' ).val( data[0].state );
                    jQuery( '#odfl_dropship_country' ).val( data[0].country );
                    window.location.href = jQuery('.odfl_add_dropship_btn').attr('href');
                    jQuery( '.already_exist' ).hide();
                    setTimeout(function(){
                        if(jQuery('.ds_popup').is(':visible')){
                            jQuery('.ds_input > input').eq(0).focus();
                        }
                    },500);
                }
            }
        }); 
        return false;
    }

    function odfl_delete_current_dropship(e)
    {
        var id = e;
        window.location.href = jQuery('.odfl_delete_dropship_btn').attr('href');
        
        jQuery('.cancel_delete').on('click', function(){
            window.location.href = jQuery('.cancel_delete').attr('href');
        });
        
        jQuery('.confirm_delete').on('click', function(){
            window.location.href = jQuery('.confirm_delete').attr('href');
            return confirm_odfl_delete_dropship(id);
        });
        return false;
    }

    function confirm_odfl_delete_dropship(e)
    {
        var postForm = {
            'action'              : 'odfl_delete_dropship',
            'dropship_delete_id'  : e
        };
                    
        jQuery.ajax({
            type      : 'POST', 
            url       : ajaxurl, 
            data      : postForm, 
            dataType  : 'json',

            success: function (data) 
            {
                if (data == 1) 
                {
                    jQuery('#row_'+e).remove();
                    jQuery( '.dropship_deleted' ).show( 'slow' ).delay(5000).hide('slow');
                    jQuery( '.dropship_updated' ).css( 'display' , 'none' );
                    jQuery( '.dropship_created' ).css( 'display' , 'none' );
                    jQuery( '.warehouse_deleted' ).css( 'display' , 'none' );
                    jQuery( '.warehouse_updated' ).css( 'display' , 'none' );
                    jQuery( '.warehouse_created' ).css( 'display' , 'none' );
                }
            }
        }); 
        return false;
    }