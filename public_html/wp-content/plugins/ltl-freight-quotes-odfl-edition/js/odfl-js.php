<?php
/**
 * Creating warehouse database table on plugin activate
 * @package     Woocommerce ODFL Edition
 * @author      <https://eniture.com/>
 * @copyright   Copyright (c) 2017, Eniture
 */
if ( ! defined( 'ABSPATH' ) ) {
    exit; 
}
add_action('admin_footer', 'odfl_ajax_carrrier_button');
/**
 * ODFL Ajax Script File
 */
function odfl_ajax_carrrier_button() 
{
?>
        <script>
            jQuery( document ).ready( function () {
              
                var url      = getUrlVarsODFL()["tab"];
                if(url === 'odfl_quotes'){
                    jQuery('#footer-left').attr('id','wc-footer-left');
                }
              
            /*
            * Add err class on connection settings page
            */   
                jQuery('.connection_section_class_odfl input[type="text"]' ).each( function () 
                {
                    if( jQuery( this ).parent().find( '.err' ).length < 1 ){
                        jQuery( this ).after( '<span class="err"></span>' );
                    }  
                });
                    
                
            /*
             * Show Note Message on Connection Settings Page
             */ 
            
                jQuery( '.connection_section_class_odfl .form-table' ).before("<div class='warning-msg'><p>Note! You must have an Old Dominion Freight Lines account to use this application. if you don't have one, contact Old Dominion Freight Lines at 800-235-5569, or email <a href='mailto:customer.service@odfl.com' target='_parent' >customer.service@odfl.com</a>.</p></div>");
            
            
            /*
             * Add maxlength Attribute on Handling Fee Quote Setting Page
             */    
            
                jQuery("#odfl_handling_fee").attr('maxlength','8');
                
            /*
             * Add maxlength Attribute on Account Number Connection Setting Page
             */    
            
                jQuery("#wc_settings_odfl_account_no").attr('maxlength','8');

            /*
             * Add Title To Connection Setting Fields
             */    
                jQuery( '#wc_settings_odfl_username' ).attr( 'title','Username' );
                jQuery( '#wc_settings_odfl_password' ).attr( 'title','Password' );
                jQuery( '#wc_settings_odfl_account_no' ).attr( 'title','Account Number' );
                jQuery( '#wc_settings_odfl_plugin_licence_key' ).attr( 'title','Plugin License Key ' );

            /*
             * Add Title To Qoutes Setting Fields
             */

                jQuery( '#odfl_label_as' ).attr( 'title','Label As' );
                jQuery( '#odfl_handling_fee' ).attr( 'title','Handling Fee / Markup' );
                
            });
            
            jQuery( ".connection_section_class_odfl .button-primary" ).click( function() {
                var input = validateInput( '.connection_section_class_odfl' );
                if( input === false )
                   return false;
                
            });
            
            jQuery( ".connection_section_class_odfl .woocommerce-save-button" ).before( '<a href="javascript:void(0)" class="button-primary odfl_test_connection">Test Connection</a>' );
        
        /**
         * ODFL Test Connection Form Valdating ajax Request
         */
    
            jQuery( '.odfl_test_connection' ).click( function (e) 
            {
                var input = validateInput( '.connection_section_class_odfl' );
                if( input === false )
                   return false;
                
                var postForm = {
                    'action'	           : 'odfl_action',
                    'odfl_username'	   : jQuery( '#wc_settings_odfl_username' ).val(),
                    'odfl_password'        : jQuery( '#wc_settings_odfl_password' ).val(),
                    'odfl_accountno'       : jQuery( '#wc_settings_odfl_account_no' ).val(),
                    'odfl_plugin_license'  : jQuery( '#wc_settings_odfl_plugin_licence_key' ).val()
                };
                               
                jQuery.ajax({
                    type      : 'POST', 
                    url       : ajaxurl, 
                    data      : postForm, 
                    dataType  : 'json',
                    
                    beforeSend : function () 
                    {
                        jQuery( ".connection_save_button" ).remove();
                        jQuery( '#wc_settings_odfl_username' ).css('background', 'rgba(255, 255, 255, 1) url("<?php echo plugins_url(); ?>/ltl-freight-quotes-odfl-edition/asset/processing.gif") no-repeat scroll 50% 50%');
                        jQuery( '#wc_settings_odfl_password' ).css('background', 'rgba(255, 255, 255, 1) url("<?php echo plugins_url(); ?>/ltl-freight-quotes-odfl-edition/asset/processing.gif") no-repeat scroll 50% 50%');
                        jQuery( '#wc_settings_odfl_account_no' ).css('background', 'rgba(255, 255, 255, 1) url("<?php echo plugins_url(); ?>/ltl-freight-quotes-odfl-edition/asset/processing.gif") no-repeat scroll 50% 50%');
                        jQuery( '#wc_settings_odfl_plugin_licence_key' ).css('background', 'rgba(255, 255, 255, 1) url("<?php echo plugins_url(); ?>/ltl-freight-quotes-odfl-edition/asset/processing.gif") no-repeat scroll 50% 50%');
                    },
                    success : function (data) 
                    {
                        jQuery( '#wc_settings_odfl_username' ).css('background', '#fff');
                        jQuery( '#wc_settings_odfl_password' ).css('background', '#fff');
                        jQuery( '#wc_settings_odfl_account_no' ).css('background', '#fff');
                        jQuery('#wc_settings_odfl_plugin_licence_key').css('background', '#fff');
                        
                        jQuery( ".odfl_success_message" ).remove();
                        jQuery( ".odfl_error_message" ).remove();
                        jQuery( "#message" ).remove();
                        
                        if ( data.message === "success" ) 
                        {
                           jQuery( '.warning-msg' ).before('<div class="notice notice-success odfl_success_message"><p><strong>Success! The test resulted in a successful connection.</strong></p></div>');
                        }  
                        else if ( data.message !== "failure" && data.message !== "success" )
                        {
                            jQuery( '.warning-msg' ).before('<div class="notice notice-error odfl_error_message"><p>Error!  '+data.message+' </p></div>');
                        }
                        else 
                        {
                            jQuery( '.warning-msg' ).before('<div class="notice notice-error odfl_error_message"><p>Error! Please verify credentials and try again.</p></div>');
                        }
                    }
                });
                e.preventDefault();
            });
        
        /**
         * ODFL Qoute Settings Tabs Validation
         */
          
            jQuery( '.quote_section_class_odfl .button-primary' ).on( 'click', function () 
            {
                jQuery( ".updated" ).hide();
                jQuery( '.error' ).remove();
                var handling_fee = jQuery( '#odfl_handling_fee' ).val();
                
                if( handling_fee.slice( handling_fee.length-1 ) == '%' ){ 
                    handling_fee = handling_fee.slice( 0, handling_fee.length-1 );
                }
                
                if( handling_fee === "" ){ 
                   return true;
                }
                else
                {      
                    if( isValidNumber( handling_fee ) === false )
                    {
                        jQuery( "#mainform .quote_section_class_odfl" ).prepend( '<div id="message" class="error inline odfl_handlng_fee_error"><p><strong>Handling fee format should be 100.2000 or 10%.</strong></p></div>' );
                        jQuery( 'html, body' ).animate({
                            'scrollTop' : jQuery( '.odfl_handlng_fee_error' ).position().top
                        }); 
                        return false;
                    }
                    else if( isValidNumber( handling_fee ) === 'decimal_point_err' ) 
                    {
                        jQuery( "#mainform .quote_section_class_odfl" ).prepend( '<div id="message" class="error inline odfl_handlng_fee_error"><p><strong>Handling fee format should be 100.2000 or 10% and only 4 digits are allowed after decimal point.</strong></p></div>' );
                        jQuery( 'html, body' ).animate({
                            'scrollTop' : jQuery( '.odfl_handlng_fee_error' ).position().top
                        }); 
                        return false;
                    }
                    else{
                        return true;
                    }
                }
            });
         
        /**
        * Read a page's GET URL variables and return them as an associative array.
        */
        function getUrlVarsODFL()
        {
            var vars = [], hash;
            var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
            for(var i = 0; i < hashes.length; i++)
            {
                hash = hashes[i].split('=');
                vars.push(hash[0]);
                vars[hash[0]] = hash[1];
            }
            return vars;
        }
         
         
    /**
     * ODFL Form Validating Inputs
     * @param form_id
     * @return string  
     */
        function validateInput( form_id )
        {   
            var has_err = true;
            jQuery( form_id+" input[type='text']" ).each( function () {
                
                var input       = jQuery( this ).val(); 
                var response    = validateString( input );
                var errorText   = jQuery( this ).attr( 'title' );
                var optional    = jQuery( this ).data( 'optional' );
                    
                var errorElement  = jQuery( this ).parent().find( '.err' );
                jQuery(errorElement).html('');
                    
                optional        = ( optional === undefined ) ? 0 : 1;
                errorText       = ( errorText != undefined ) ? errorText : '';
                
                if( ( optional == 0 ) && ( response  == false ||  response  == 'empty' ) ) 
                {
                    errorText = ( response  == 'empty' ) ? errorText+' is required.':'Invalid input.';
                    jQuery( errorElement ).html( errorText ); 
                }
                has_err = ( response != true && optional == 0 )? false : has_err; 
            });
            return has_err;
        }
    /**
     * ODFL Validating Numbers 
     */    
        function isValidNumber( value, noNegative )
        {
            if ( typeof( noNegative )==='undefined' ) noNegative = false;
            var isValidNumber   = false;
            var validNumber     = ( noNegative == true ) ? parseFloat( value ) >= 0 : true;
            if(( value == parseInt( value ) || value == parseFloat( value ) ) && ( validNumber ) ) 
            {
                if(value.indexOf(".")>=0)
                {
                    var n = value.split(".");
                    if(n[n.length-1].length <=4){
                       isValidNumber = true;
                   }else{
                       isValidNumber = 'decimal_point_err';
                    }
                }
                else{
                   isValidNumber = true;
                }
            }
            return isValidNumber;
        }
        
    /**
     * ODFL Validating String 
     */    
        function validateString( string )
        {
            if(string == '')
                return 'empty';
            else
                return true;
            
        }
        </script>
        <?php
    }