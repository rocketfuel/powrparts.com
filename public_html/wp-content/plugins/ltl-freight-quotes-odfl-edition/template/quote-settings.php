<?php
/**
 * ODFL wooCommerce Quote settings html form template
 * @package     Woocommerce ODFL Edition
 * @author      <https://eniture.com/>
 * @copyright   Copyright (c) 2017, Eniture
 */
if ( ! defined( 'ABSPATH' ) ) {
    exit; 
}

add_action('admin_footer', 'odfl_quote_btn_text');
/**
 * ODFL JQuery Scripts for Giving name to the button
 */
function odfl_quote_btn_text() 
{
    echo '<script>
            jQuery( document ).ready(function() {
                jQuery(".quote_section_class_odfl .button-primary").val("Save Changes");
                jQuery(".connection_section_class_odfl .button-primary").val("Save Changes");
            });
        </script>';
}
   
 /**
 * ODFL Quote Settings Class
 */
    
    class ODFL_Quote_Settings 
    {
        /**
         * ODFL Quote Settings
         * @return array
         */
        function odfl_quote_settings_tab() 
        {
            echo '<div class="quote_section_class_odfl">';
            $settings = array(
                'section_title_quote' => array(
                    'title'    => __('Quote Settings ', 'woocommerce_odfl_quote'),
                    'type'     => 'title',
                    'desc'     => '',
                    'id'       => 'odfl_section_title_quote'
                ),

                'label_as_odfl' => array(
                    'name'     => __('Label As ', 'woocommerce_odfl_quote'),
                    'type'     => 'text',
                    'desc'     => '<span class="desc_text_style">What the user sees during checkout, e.g. "LTL Freight". If left blank, "Freight" will display as the shipping method.</span>',
                    'id'       => 'odfl_label_as' 
                ),

                'odfl_show_delivery_estimate' => array(
                    'name'     => __('Show Delivery Estimate ', 'woocommerce_odfl_quote'),
                    'type'     => 'checkbox',
                    'desc'     => __('', 'woocommerce_odfl_quote'),
                    'id'       => 'odfl_delivey_estimate',
                    'class'    => 'odflCheckboxClass'
                ),
                
                'accessorial_quoted_odfl' => array(
                    'title'     => __('', 'woocommerce'),
                    'name'      => __('', 'woocommerce_odfl_quote'),
                    'desc'      => '',
                    'id'        => 'woocommerce_accessorial_quoted_odfl',
                    'css'       => '',
                    'default'   => '',
                    'type'      => 'title',
                ),

                'accessorial_odfl' => array(
                    'name'      => __('Services to include in Quoted Price ', 'woocommerce_odfl_quote'),
                    'type'      => 'title',
                    'desc'      => '',
                    'id'        => 'wc_settings_odfl_accessorial'
                ),
                
                'accessorial_quoted_odfl' => array(
                    'title'      => __('', 'woocommerce'),
                    'name'       => __('', 'woocommerce_odfl_quote'),
                    'desc'       => '',
                    'id'         => 'woocommerce_odfl_accessorial_quoted',
                    'css'        => '',
                    'default'    => '',
                    'type'       => 'title',
                ),

                'accessorial_residential_delivery_odfl' => array(
                    'name'     => __('Residential Delivery ', 'woocommerce_odfl_quote'),
                    'type'     => 'checkbox',
                    'desc'     => __('', 'woocommerce_odfl_quote'),
                    'id'       => 'odfl_residential',
                    'class'    => 'accessorial_service odflCheckboxClass',
                ),

                'accessorial_liftgate_delivery_odfl' => array(
                    'name'     => __('Lift Gate Delivery ', 'woocommerce_odfl_quote'),
                    'type'     => 'checkbox',
                    'desc'     => __('', 'woocommerce_odfl_quote'),
                    'id'       => 'odfl_liftgate',
                    'class'    => 'accessorial_service odflCheckboxClass',
                ),
                
                'handing_fee_markup_odfl' => array(
                    'name'     => __('Handling Fee / Markup ', 'woocommerce_odfl_quote'),
                    'type'     => 'text',
                    'desc'     => '<span class="desc_text_style">Amount excluding tax. Enter an amount, e.g 3.75, or a percentage, e.g, 5%. Leave blank to disable.</span>',
                    'id'       => 'odfl_handling_fee'
                ),
                
                'allow_other_plugins_odfl' => array(
                    'name'      => __('Show WooCommerce Shipping Options ', 'woocommerce_odfl_quote'),
                    'type'      => 'select',
                    'default'   => '3',
                    'desc'      => __('<span class="desc_text_style">Enabled options on WooCommerce Shipping page are included in quote results.</span>', 'woocommerce_odfl_quote'),
                    'id'        => 'odfl_allow_other_plugins',
                    'options'   => array(
                        'no'      => __('NO', 'NO'),
                        'yes'     => __('YES', 'YES')
                    )
                ),
                
                'return_ODFL_quotes' => array(
                    'name'   => __('Return ODFL Freight quotes when an order parcel shipment weight exceeds 150 lbs ', 'woocommerce-settings-odfl_quetes'),
                    'type'   => 'checkbox',
                    'desc'   => '<span class="desc_text_style">When checked, the ODFL Freight plugin will return quotes when an order’s total weight exceeds 150 lbs (the maximum permitted by FedEx and UPS), even if none of the products have settings to indicate that it will ship LTL Freight.  To increase the accuracy of the returned quote(s), all products should have accurate weights and dimensions. </span>',
                    'id'     => 'en_plugins_return_LTL_quotes',
                   'class'   => 'odflCheckboxClass'
                ),
                
                'section_end_quote' => array(
                    'type'    => 'sectionend',
                    'id'      => 'odfl_quote_section_end'
                )
            );
            return $settings;
        }
    }