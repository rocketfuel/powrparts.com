<?php
/**
 * ODFL Dropship form and view list
 * @package     Woocommerce ODFL Edition
 * @author      <https://eniture.com/>
 * @copyright   Copyright (c) 2017, Eniture
 */
if ( ! defined( 'ABSPATH' ) ) {
    exit; 
}

$dropship_list = $wpdb->get_results(
    "SELECT id, city, state, zip, country, location, nickname
    FROM ".$wpdb->prefix."warehouse WHERE location = 'dropship'"
);
?>
<script type="text/javascript">
    jQuery( document ).ready(function() {
        jQuery( '.hide_drop_val' ).click( function () 
        {
            jQuery( '#edit_dropship_form_id' ).val( '' );
            jQuery( "#odfl_dropship_nickname" ).val( '' );
            jQuery( "#odfl_dropship_zip" ).val( '' );
            jQuery( '.city_select' ).hide();
            jQuery( '.city_input' ).show();
            jQuery( '#odfl_dropship_city' ).css( 'background', 'none' );
            jQuery( "#odfl_dropship_city" ).val( '' );
            jQuery( '.odfl_multi_state' ).empty();
            jQuery( "#odfl_dropship_state" ).val( '' );
            jQuery( "#odfl_dropship_country" ).val( '' );
            jQuery( '.odfl_zip_validation_err' ).hide();
            jQuery( '.odfl_city_validation_err' ).hide();
            jQuery( '.odfl_state_validation_err' ).hide();
            jQuery( '.odfl_country_validation_err' ).hide();
            jQuery( '.not_allowed' ).hide();
            jQuery( '.already_exist' ).hide();
            jQuery( '.odfl_dropship_invalid_input_message' ).hide();
            jQuery( '.wrng_credential' ).hide();
        });

        jQuery('.odfl_add_dropship_btn').click(function()
        {
            setTimeout(function(){
                if(jQuery('.ds_popup').is(':visible')){
                    jQuery('.ds_input > input').eq(0).focus();
                }
            },500);
        });


        jQuery( "#odfl_dropship_zip" ).on('change', function() {
            
        if (jQuery( "#odfl_dropship_zip" ).val() == '') {
            return false;
        }
        
        jQuery( '#odfl_dropship_city' ).css('background', 'rgba(255, 255, 255, 1) url("<?php echo plugins_url(); ?>/ltl-freight-quotes-odfl-edition/asset/processing.gif") no-repeat scroll 50% 50%');
        jQuery( '#odfl_dropship_state' ).css('background', 'rgba(255, 255, 255, 1) url("<?php echo plugins_url(); ?>/ltl-freight-quotes-odfl-edition/asset/processing.gif") no-repeat scroll 50% 50%');
        jQuery( '.city_select_css' ).css('background', 'rgba(255, 255, 255, 1) url("<?php echo plugins_url(); ?>/ltl-freight-quotes-odfl-edition/asset/processing.gif") no-repeat scroll 50% 50%');
        jQuery( '#odfl_dropship_country' ).css('background', 'rgba(255, 255, 255, 1) url("<?php echo plugins_url(); ?>/ltl-freight-quotes-odfl-edition/asset/processing.gif") no-repeat scroll 50% 50%');

        var postForm = {
            'action'      : 'odfl_get_address',
            'origin_zip'  : jQuery('#odfl_dropship_zip').val()
        };

        jQuery.ajax({
            type      : 'POST', 
            url       : ajaxurl, 
            data      : postForm, 
            dataType  : 'json',

            beforeSend : function () 
            {
                jQuery( '.odfl_zip_validation_err' ).hide();
                jQuery( '.odfl_city_validation_err' ).hide();
                jQuery( '.odfl_state_validation_err' ).hide();
                jQuery( '.odfl_country_validation_err' ).hide();
            },
            success : function (data) 
            {
                if( data )
                {
                    if( data.country === 'US' || data.country === 'CA' )
                    {
                        if (data.postcode_localities == 1) 
                        {
                            jQuery( '.city_select' ).show();
                            jQuery( '#dropship_actname' ).replaceWith( data.city_option );
                            jQuery( '#odfl_dropship_state' ).val( data.state );
                            jQuery( '#odfl_dropship_country' ).val( data.country );
                            jQuery( '.city-multiselect' ).change( function(){
                                setCity(this);
                            });
                            jQuery( '#odfl_dropship_city' ).val( data.first_city );
                            jQuery( '#odfl_dropship_state' ).css('background', 'none');  
                            jQuery( '.city_select_css' ).css('background', 'none'); 
                            jQuery( '#odfl_dropship_country' ).css('background', 'none');  
                            jQuery( '.city_input' ).hide();
                        }
                        else
                        {
                            jQuery( '.city_input' ).show();
                            jQuery( '#_city' ).removeAttr('value');
                            jQuery( '.city_select' ).hide();
                            jQuery( '#odfl_dropship_city' ).val( data.city );
                            jQuery( '#odfl_dropship_state' ).val( data.state );
                            jQuery( '#odfl_dropship_country' ).val( data.country );
                            jQuery( '#odfl_dropship_city' ).css('background', 'none');
                            jQuery( '#odfl_dropship_state' ).css('background', 'none');  
                            jQuery( '#odfl_dropship_country' ).css('background', 'none');  
                        }
                    }
                    else if( data.result === 'false' )
                    {
                        jQuery( '.not_allowed' ).show('slow');
                        jQuery( '#odfl_dropship_city' ).css('background', 'none');
                        jQuery( '#odfl_dropship_state' ).css('background', 'none');
                        jQuery( '#odfl_dropship_country' ).css('background', 'none');    
                        setTimeout(function () {
                            jQuery('.not_allowed').hide('slow');
                        }, 5000);
                    }
                    else if( data.apiResp === 'apiErr' )
                    {
                        jQuery( '.wrng_credential' ).show('slow');
                        jQuery( '#odfl_dropship_city' ).css('background', 'none');
                        jQuery( '#odfl_dropship_state' ).css('background', 'none');
                        jQuery( '#odfl_dropship_country' ).css('background', 'none');    
                        setTimeout(function () {
                            jQuery('.wrng_credential').hide('slow');
                        }, 5000);
                    }
                    else
                    {
                        jQuery( '.not_allowed' ).show('slow');
                        jQuery( '#odfl_dropship_city' ).css('background', 'none');
                        jQuery( '#odfl_dropship_state' ).css('background', 'none');
                        jQuery( '#odfl_dropship_country' ).css('background', 'none');    
                        setTimeout(function () {
                            jQuery('.not_allowed').hide('slow');
                        }, 5000);
                    }
                }
            }
        }); 
        return false;
    });
    });
  
    function setCity(e)
    {
      var city = jQuery(e).val();
      jQuery('#odfl_dropship_city').val(city);
    }
    
    jQuery(function() {
        jQuery('input.alphaonly').keyup(function() {
            if (this.value.match(/[^a-zA-Z ]/g)) {
                this.value = this.value.replace(/[^a-zA-Z ]/g, '');
            }
        });
    });
</script>
    <a href="#odfl_delete_dropship_btn" class="odfl_delete_dropship_btn hide_drop_val"></a>
    <div id="odfl_delete_dropship_btn" class="odfl_warehouse_overlay">
        <div class="odfl_add_warehouse_popup">
            <h2 class="del_hdng">
                Warning!
            </h2>
            <p class="delete_p">
                Warning! If you delete this location, Drop ship location settings will be disable against products if any.
            </p>
            <div class="del_btns">
                <a href="#" class="cancel_delete">Cancel</a>
                <a href="#" class="confirm_delete">OK</a>
            </div>
        </div>
    </div>
    <h1>Drop ships</h1><br>
    <a href="#add_dropship_btn" title="Add Drop ship" class="odfl_add_dropship_btn hide_drop_val hoveraffect">Add</a>
    <br>
    <div class="warehouse_text">
        <p>Locations that inventory specific items that are drop shipped to the destination. Use the product's settings page to identify it as a drop shipped item and its associated drop ship location. Orders that include drop shipped items will display a single figure for the shipping rate estimate that is equal to the sum of the cheapest option of each shipment required to fulfill the order.</p>
    </div>
    <div id="message" class="updated inline dropship_created">
        <p><strong>Success! New drop ship added successfully.</strong></p>
    </div>
    <div id="message" class="updated inline dropship_updated">
        <p><strong>Success! Drop ship updated successfully.</strong></p>
    </div>
    <div id="message" class="updated inline dropship_deleted">
        <p><strong>Success! Drop ship deleted successfully.</strong></p>
    </div>
    <table class="odfl_dropship_list" id="append_dropship">
        <thead>
            <tr>
                <th class="odfl_dropship_list_heading">Nickname</th>
                <th class="odfl_dropship_list_heading">City</th>
                <th class="odfl_dropship_list_heading">State</th>
                <th class="odfl_dropship_list_heading">Zip</th>
                <th class="odfl_dropship_list_heading">Country</th>
                <th class="odfl_dropship_list_heading">Action</th>
            </tr>
        </thead>
        <tbody>
<?php
    if ( count( $dropship_list ) > 0 ) 
    {
        foreach ( $dropship_list as $list ) 
        {
?>
            <tr id="row_<?php echo ( isset( $list->id ) ) ? esc_attr( $list->id ) : ''; ?>">
                <td class="odfl_dropship_list_data"><?php echo ( isset( $list->nickname ) ) ? esc_attr( $list->nickname ) : ''; ?></td>
                <td class="odfl_dropship_list_data"><?php echo ( isset( $list->city ) )     ? esc_attr( $list->city )     : ''; ?></td>
                <td class="odfl_dropship_list_data"><?php echo ( isset( $list->state ) )    ? esc_attr( $list->state )    : ''; ?></td>
                <td class="odfl_dropship_list_data"><?php echo ( isset( $list->zip ) )      ? esc_attr( $list->zip )      : ''; ?></td>
                <td class="odfl_dropship_list_data"><?php echo ( isset( $list->country ) )  ? esc_attr( $list->country )  : ''; ?></td>
                <td class="odfl_dropship_list_data">
                <a href="javascript(0)" onclick="return odfl_edit_dropship(<?php echo ( isset( $list->id ) ) ? esc_attr( $list->id ) : ''; ?>);"><img src="<?php echo plugins_url(); ?>/ltl-freight-quotes-odfl-edition/asset/edit.png" title="Edit"></a>
                <a href="javascript(0)" onclick="return odfl_delete_current_dropship(<?php echo ( isset( $list->id ) ) ? esc_attr( $list->id ) : ''; ?>);"><img src="<?php echo plugins_url(); ?>/ltl-freight-quotes-odfl-edition/asset/delete.png" title="Delete"></a></td>
            </tr>
<?php 
        } 
    }
    else
    { 
?>
            <tr class="new_dropship_add" data-id=0></tr>
<?php 
    } 
?>
        </tbody>
    </table>

    <!-- Add Popup for new dropship -->
    <div id="add_dropship_btn" class="odfl_warehouse_overlay">
        <div class="odfl_add_warehouse_popup ds_popup">
            <h2 class="dropship_heading">Drop ship</h2>
            <a class="close" href="#">&times;</a>
            <div class="content">
                <div class="already_exist">
                    <strong>Error!</strong> Zip code already exists.
                </div>
                <div class="odfl_dropship_invalid_input_message">
                    <strong> Error! </strong> Invalid input data.
                </div>
                <div class="not_allowed">
                    <p><strong>Error!</strong> Please enter US / CA zip code.</p>
                </div>
                <div class="wrng_credential">
                    <p><strong>Error!</strong> Please verify credentials at connection settings panel.</p>
                </div>
                <form method="post">
                    <input type="hidden" name="edit_dropship_form_id" value="" id="edit_dropship_form_id">
                    <div class="odfl_add_warehouse_input ds_input">
                        <label for="odfl_dropship_nickname">Nickname</label>
                        <input type="text" title="Nickname" value="" name="odfl_dropship_nickname" placeholder="Nickname" id="odfl_dropship_nickname">
                     </div>
                    <div class="odfl_add_warehouse_input">
                        <label for="odfl_dropship_zip">Zip</label>
                        <input type="text" value="" title="Zip" name="odfl_dropship_zip" maxlength="7" placeholder="30214" id="odfl_dropship_zip">
                    </div>
              
                    <div class="odfl_add_warehouse_input city_input">
                        <label for="odfl_dropship_city">City</label>
                        <input type="text" value="" title="City" name="odfl_dropship_city" placeholder="Fayetteville" id="odfl_dropship_city">
                    </div>
              
                    <div class="odfl_add_warehouse_input city_select" style="display:none;">
                        <label for="odfl_dropship_city">City</label>
                        <select id="dropship_actname" value=""></select>
                    </div>
              
                    <div class="odfl_add_warehouse_input">
                        <label for="odfl_dropship_state">State</label>
                        <input type="text" value="" title="State" maxlength="2" name="odfl_dropship_state" placeholder="GA" id="odfl_dropship_state">
                    </div>
              
                    <div class="odfl_add_warehouse_input">
                        <label for="odfl_dropship_country">Country</label>
                        <input type="text" name="odfl_dropship_country" title="Country" maxlength="2" value="" placeholder="US" id="odfl_dropship_country">
                        <input type="hidden" name="odfl_dropship_location" value="dropship" id="odfl_dropship_location">
                    </div>
              
                    <input type="submit" name="odfl_submit_dropship" value="Save" class="save_warehouse_form" onclick="return save_odfl_dropship();">
                </form>
            </div>
        </div>
    </div>
     