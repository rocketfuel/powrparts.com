<?php
/**
 * ODFL Product Details Setting Page For Shipping
 * @package     Woocommerce ODFL Edition
 * @author      <https://eniture.com/>
 * @copyright   Copyright (c) 2017, Eniture
 */
if ( ! defined( 'ABSPATH' ) ) {
    exit; 
}    

    if( ! has_filter( 'En_Plugins_variable_freight_classification_filter' ) )
    {
        add_action( 'woocommerce_product_after_variable_attributes', 'odfl_variation_freight_classification', 20, 3 );
        add_filter( 'En_Plugins_variable_freight_classification_filter', 'odfl_freight_class_filter', 20, 1 );
    }
       
    /**
     * ODFL Variation Freight Classification
     * @param $freight_clasification
     * @return string
     */
    function odfl_variable_freight_class_filter( $freight_clasification ) {
        return $freight_clasification;
    }
      
    /**
     * ODFL Freight Classification List Variations
     * @param $loop
     * @param $variation_data
     * @param $variation
     * @return array
     */
    function odfl_variation_freight_classification($loop, $variation_data, $variation) 
    {
        $freight_clasification  = woocommerce_wp_select(
            array(
                'id'        => '_ltl_freight_variation[' . $variation->ID . ']',
                'label'     => __('Freight classification <br>', 'woocommerce'),
                'value'     => get_post_meta($variation->ID, '_ltl_freight_variation', true),
                'options'   => array(
                    'get_parent'      => __('Same as parent', 'woocommerce'),
                    '50'              => __('50', 'woocommerce'),
                    '55'              => __('55', 'woocommerce'),
                    '60'              => __('60', 'woocommerce'),
                    '65'              => __('65', 'woocommerce'),
                    '70'              => __('70', 'woocommerce'),
                    '77.5'            => __('77.5', 'woocommerce'),
                    '85'              => __('85', 'woocommerce'),
                    '92.5'            => __('92.5', 'woocommerce'),
                    '100'             => __('100', 'woocommerce'),
                    '110'             => __('110', 'woocommerce'),
                    '125'             => __('125', 'woocommerce'),
                    '150'             => __('150', 'woocommerce'),
                    '175'             => __('175', 'woocommerce'),
                    '200'             => __('200', 'woocommerce'),
                    '225'             => __('225', 'woocommerce'),
                    '250'             => __('250', 'woocommerce'),
                    '300'             => __('300', 'woocommerce'),
                    '400'             => __('400', 'woocommerce'),
                    '500'             => __('500', 'woocommerce'),
                    'DensityBased'    => __('Density Based', 'woocommerce')
                )
            )
        );
        apply_filters( 'En_Plugins_variable_freight_classification_filter', $freight_clasification );
    } 
        
    if( ! has_filter( 'En_Plugins_freight_classification_filter' ) )
    {
        add_action( 'woocommerce_product_options_shipping', 'odfl_freight_class', 20 );
        add_filter( 'En_Plugins_freight_classification_filter', 'odfl_freight_class_filter' , 20, 1 );
    }

    /**
     * ODFL Parent Freight
     * @param $freight_clasification
     * @return string
     */
    function odfl_freight_class_filter( $freight_clasification ) {
        return $freight_clasification;
    }
       
    /**
     * ODFL Freight Classification List Simple
     * @return array
     */
    function odfl_freight_class() 
    {
       $freight_clasification =  woocommerce_wp_select(
            array(
                'id'         => '_ltl_freight',
                'label'      => __('Freight classification', 'woocommerce'),
                'options'    => array(
                    ''                => __('No Freight Class', 'woocommerce'),
                    '50'              => __('50', 'woocommerce'),
                    '55'              => __('55', 'woocommerce'),
                    '60'              => __('60', 'woocommerce'),
                    '65'              => __('65', 'woocommerce'),
                    '70'              => __('70', 'woocommerce'),
                    '77.5'             => __('77.5', 'woocommerce'),
                    '85'              => __('85', 'woocommerce'),
                    '92.5'             => __('92.5', 'woocommerce'),
                    '100'             => __('100', 'woocommerce'),
                    '110'             => __('110', 'woocommerce'),
                    '125'             => __('125', 'woocommerce'),
                    '150'             => __('150', 'woocommerce'),
                    '175'             => __('175', 'woocommerce'),
                    '200'             => __('200', 'woocommerce'),
                    '225'             => __('225', 'woocommerce'),
                    '250'             => __('250', 'woocommerce'),
                    '300'             => __('300', 'woocommerce'),
                    '400'             => __('400', 'woocommerce'),
                    '500'             => __('500', 'woocommerce'),
                    'DensityBased'    => __('Density Based', 'woocommerce')
                )
            )
        );	
        apply_filters( 'En_Plugins_freight_classification_filter', $freight_clasification );
    }


    /**
     * ODFL Dropship Checkbox
     * @global $wpdb
     * @param $loop
     * @param $variation_data
     * @param $variation
     * @return Dropship List In loop on Different Variation
     */
    function odfl_dropship($loop, $variation_data = array(), $variation = array()) 
    {
        global $wpdb;
        $dropship_list = $wpdb->get_results("SELECT id, city, state, zip, country, location, nickname FROM ".$wpdb->prefix."warehouse WHERE location = 'dropship'");

        if(!empty($dropship_list))
        {
            ( isset( $variation->ID ) && $variation->ID > 0 ) ? $variationID = $variation->ID : $variationID = get_the_ID();

            woocommerce_wp_checkbox(
                array(
                    'id'     => '_enable_dropship['.$variationID.']',
                    'label'  => __('Enable drop ship location', 'woocommerce'),
                    'value'  => get_post_meta( $variationID, '_enable_dropship', true ),
                )
            );

            $attributes = array(
                'id'     => '_dropship_location['.$variationID.']',
                'class'  => 'p_ds_location',
            );

            $get_loc   = maybe_unserialize(get_post_meta($variationID, '_dropship_location', true));

            $valuesArr = array();
            foreach ($dropship_list as $list) 
            {
                ( isset($list->nickname) && $list->nickname == '' ) ? $nickname = '' : $nickname = $list->nickname . ' - ';
                ( isset($list->country) && $list->country == '' )   ? $country  = '' : $country  = '(' .$list->country. ')';

                $location                    = $nickname . $list->zip . ', ' . $list->city . ', ' . $list->state .' ' . $country;
                $finalValue['option_id']     = $list->id;
                $finalValue['option_value']  = $list->id;
                $finalValue['option_label']  = $location;
                $valuesArr[]                 = $finalValue;
            }

            $aFields[] = array(
                'attributes'      =>$attributes,
                'label'           => 'Drop ship location',
                'value'           => $valuesArr,
                'name'            => '_dropship_location['.$variationID.'][]',
                'type'            =>'select',
                'selected_value'  => $get_loc,
                'variant_id'      => $variationID
            );

            $aFields   = apply_filters('before_wwe_ltl_product_detail_fields', $aFields);
            apply_filters('En_Plugins_dropship_filter', $aFields,$get_loc, $variationID);
        }
    }

    
    if( ! has_filter( 'En_Plugins_dropship_filter' ) ) 
    {
        add_action( 'woocommerce_product_options_shipping', 'odfl_dropship' );
        add_action( 'woocommerce_product_after_variable_attributes', 'odfl_dropship', 10, 3 );
        add_filter( 'En_Plugins_dropship_filter','odfl_dropship_filter', 10, 3 );
    }

    /**
     * Dropship Filter
     * @param $aFields
     * @param $get_loc
     * @param $variationID
     */
    function odfl_dropship_filter( $aFields, $get_loc, $variationID ) 
    {
        $fieldsHtml = '';
        foreach ($aFields as $key => $sField) 
        {
            $sField      = apply_filters( 'wwe_ltl_product_detail_fields', $sField );
            $fieldsHtml  = odfl_dropship_html($sField, $fieldsHtml, $get_loc, $variationID);
        }
        $fieldsHtml = apply_filters('after_wwe_ltl_product_detail_fields', $fieldsHtml);
        echo $fieldsHtml;
    } 

    /**
     * ODFL Save Custom Genral Feilds
     * @param $post_id
     */
    function odfl_woo_add_custom_general_fields_save($post_id) 
    { 
        $woocommerce_select     = ( isset($_POST['_ltl_freight']) )                  ? $_POST['_ltl_freight']                  : "";
        $woocommerce_checkbox   = ( isset($_POST['_enable_dropship'][$post_id]) )    ? $_POST['_enable_dropship'][$post_id]    : "";;
        $woocommerce_hazardous  = ( isset($_POST['_hazardousmaterials'][$post_id]) ) ? $_POST['_hazardousmaterials'][$post_id] : "";  
        $dropship_location      = $_POST['_dropship_location'][$post_id];
        $dropship_location_val  = array_map( 'intval', $dropship_location );

        update_post_meta($post_id, '_ltl_freight', esc_attr($woocommerce_select));
        update_post_meta($post_id, '_enable_dropship', esc_attr($woocommerce_checkbox));
        update_post_meta($post_id, '_dropship_location', maybe_serialize($dropship_location_val));
        update_post_meta($post_id, '_hazardousmaterials', esc_attr($woocommerce_hazardous));
    }

    /**
     * Drop Ship Dropdown Select
     * @param $sField
     * @param $fieldsHtml
     * @param $get_loc
     * @param $variantId
     * @return string
     */
    function odfl_dropship_html($sField, $fieldsHtml,$get_loc, $variantId) 
    {
        $str = odfl_attributes_string($sField['attributes']);

        $fieldsHtml  .= '<p class="form-field _dropship_location">';
        $fieldsHtml  .= '<label for="_dropship_location">'.$sField['label'].'</label>';

        if($sField['type'] == 'select')
        {
            $fieldsHtml .= '<select name="'.$sField['name'].'" '.$str.'>';
            if ($sField['value']) 
            {
                foreach ($sField['value'] as $option) 
                {
                    $selected_option  = odfl_product_ds_selected_option($sField['selected_value'], $option['option_value']);
                    $fieldsHtml      .= '<option value="'.esc_attr($option['option_value']).'" '.$selected_option.'>'.esc_html($option['option_label']).' </option>';
                }
            }
            $fieldsHtml .= '</select>';
        }
        $fieldsHtml .= '</p>';
        return $fieldsHtml;
    }


    /**
     * ODFL Attribute String
     * @param $attributes
     * @return string
     */
    function odfl_attributes_string($attributes) 
    {
        $str = '';
        foreach($attributes as $key=>$sAttribute){
            $str .= ' '.$key.' ="'.$sAttribute.'" ';                                        
        }
        return $str;
    }  

    /**
     * ODFL Show Selected Dropship
     * @param $get_loc
     * @param $option_val
     * @return string
     */
    function odfl_product_ds_selected_option($get_loc, $option_val)
    {
        $selected = '';
        if(is_array($get_loc)){
            if(in_array($option_val, $get_loc)){
                $selected  =  'selected="selected"';
            }
        }else{
            $selected  = selected($get_loc, $option_val, false);
        }
        return $selected;
    }
 
    if( ! has_filter( 'En_Plugins_hazmet_filter' ) )
    {
        add_action( 'woocommerce_product_options_shipping', 'odfl_hazardous_materials', 30 );
        add_action( 'woocommerce_product_after_variable_attributes', 'odfl_hazardous_materials' , 30, 3 );
        add_filter( 'En_Plugins_hazmet_filter', 'odfl_hazmet_field_filter', 30, 1 );
    }
      
    /**
     * ODFL Hazardous Materials
     * @param $hazardous
     * @return string
     */
    function odfl_hazmet_field_filter( $hazardous ) {
        return $hazardous;
    }

    /**
     * ODFL Hazardous Materials
     * @global $wpdb
     * @param $loop
     * @param $variation_data
     * @param $variation
     * @retrun array
     */
    function odfl_hazardous_materials($loop, $variation_data = array(), $variation = array()) 
    {
        global $wpdb;

        ( isset( $variation->ID ) && $variation->ID > 0 ) ? $variationID = $variation->ID : $variationID = get_the_ID();

       $hazardous =  woocommerce_wp_checkbox(
            array(
                'id'     => '_hazardousmaterials['.$variationID.']',
                'label'  => __('Hazardous materials', 'woocommerce'),
                'value'  => get_post_meta( $variationID, '_hazardousmaterials', true ),
            )
        );
        apply_filters( 'En_Plugins_hazmet_filter', $hazardous );
    }    

    /**
     * ODFL Save Freight and Dropship against different Post
     * @param $post_id
     */
    function odfl_save_variable_fields($post_id) 
    {
        if (isset($post_id) && $post_id>0) :

            $_select                = ( isset($_POST['_ltl_freight_variation'][$post_id]) ) ? $_POST['_ltl_freight_variation'][$post_id] : "";
            $enable_ds              = ( isset( $_POST['_enable_dropship'][$post_id] )       ? $_POST['_enable_dropship'][$post_id]       : "");
            $enable_hazardous       = ( isset( $_POST['_hazardousmaterials'][$post_id] )    ? $_POST['_hazardousmaterials'][$post_id]    : "");
            $ds_locaton             = $_POST['_dropship_location'][$post_id];
            $dropship_location_val  = array_map( 'intval', $ds_locaton );

            update_post_meta($post_id, '_enable_dropship', esc_attr($enable_ds));
            update_post_meta($post_id, '_hazardousmaterials', esc_attr($enable_hazardous));

            if (isset($ds_locaton)) {
                update_post_meta($post_id, '_dropship_location', maybe_serialize($dropship_location_val));
            }

            if (isset($_select)) {
                update_post_meta($post_id, '_ltl_freight_variation', esc_attr($_select));
            }

        endif;
    }
