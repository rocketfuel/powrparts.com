<?php
/**
 * Add, Update or Delete Warehouse/Dropship
 * @package     Woocommerce ODFL Edition
 * @author      <https://eniture.com/>
 * @copyright   Copyright (c) 2017, Eniture
 */
if ( ! defined( 'ABSPATH' ) ) {
    exit; 
}

add_action( 'wp_ajax_odfl_get_address', 'get_odfl_address_api_ajax' );
add_action( 'wp_ajax_nopriv_odfl_get_address', 'get_odfl_address_api_ajax' );

/**
 * getting ODFL address api ajax
 */
function get_odfl_address_api_ajax () 
{
    $zipCode = ( isset( $_POST['origin_zip'] ) ) ? sanitize_text_field( preg_replace( '/\s+/', '', $_POST['origin_zip'] ) ) : "";

    if( isset( $zipCode ) )
    {        
        $zipCode              = str_replace(' ', '', $zipCode);
        $accessLevel          = 'address';
        $resp_json            = ODFL_Get_Distance::odfl_address($zipCode, $accessLevel);
        $map_result           = json_decode($resp_json, true);
        $city 	          = "";
        $state 		  = "";
        $country 	          = "";
        $postcode_localities  = 0;
        $city_option          = array();

        if(isset($map_result['error']) && !empty($map_result['error']))
        {
            echo json_encode(array('apiResp' => 'apiErr'));
            exit;
        }

        if ( count( $map_result['results'] ) == 0 ) 
        {
            echo json_encode(array('result' => 'false'));
            exit;
        }

        $first_city = '';
        if ( count( $map_result['results'] ) > 0 ) 
        {   
            $arrComponents = $map_result['results'][0]['address_components'];           
            if ( $map_result['results'][0]['postcode_localities'] ) 
            {
                foreach ( $map_result['results'][0]['postcode_localities'] as $index => $component ) 
                {  
                    $first_city    = ($index==0)?$component:$first_city;
                    $city_option  .= '<option value="' . trim($component) . ' "> ' . $component . ' </option>';
                }

                $city = '<select id="city" class="city-multiselect odfl_multi_state city_select_css" name="city" aria-required="true" aria-invalid="false">
                    ' . $city_option . '</select>';
                $postcode_localities = 1;

            } 
            elseif( $arrComponents ) 
            {
                foreach ( $arrComponents as $index => $component ) 
                {
                    $type = $component['types'][0];
                    if ( $city == "" && ( $type == "sublocality_level_1" || $type == "locality" ) ){ 
                        $city_name 	= trim($component['long_name']);
                    }
                }
            }

            if( $arrComponents )
            {
                foreach ( $arrComponents as $index => $state_app ) 
                {
                    $type = $state_app['types'][0];
                    if ( $state == "" && ( $type == "administrative_area_level_1" ) ) 
                    {
                        $state_name  = trim( $state_app['short_name'] );
                        $state       = $state_name;
                    }

                    if ( $country == "" && ( $type == "country" ) ) 
                    {
                        $country_name = trim( $state_app['short_name'] );
                        ( $country_name == 'CA' || $country_name == 'CN' || $country_name == 'CAN' ) ? $country = 'CA' : $country = $country_name;
                    }
                }
            }
            echo json_encode(array('first_city' =>$first_city, 'city' => $city_name, 'city_option' => $city, 'state' => $state, 'country' => $country, 'postcode_localities' => $postcode_localities));
            exit;
        }
    }
}
    
/**
 * Check Validation For Post Data
 * @param $sPostData
 * @return string
 */
function odfl_validating_post_data($sPostData) 
{
    foreach ($sPostData as $key => $tag) 
    {
        $check_characters = preg_match('/[#$%@^&_*!()+=\-\[\]\';,.\/{}|":<>?~\\\\]/', $tag);
        if ($check_characters != 1 ) 
        {
            if ($key === 'city' || $key === 'nickname' )
            {
                $data[$key] = sanitize_text_field($tag);
            } else {
                $data[$key] = preg_replace( '/\s+/', '', sanitize_text_field($tag));
            } 


        } else {
            $data[$key] = 'Error';
        }
    }
    return $data;
}
 
/**
 * Filtered Data Array
 * @param $validatePostData
 * @return array
 */
function odfl_filtered_data( $validatePostData ) 
{
    return array(
        'city'      => $validatePostData["city"],
        'state'     => $validatePostData["state"],
        'zip'       => preg_replace( '/\s+/', '', $validatePostData["zip"] ),
        'country'   => odfl_get_countrycode($validatePostData["country"]),
        'location'  => $validatePostData["location"],                     
        'nickname'  => ( isset( $validatePostData["nickname"] ) ) ? $validatePostData["nickname"] : "",
    );
}

/**
 * Get ODFL Country Code Function
 * @param $sCountryName
 * @return string
 */
function odfl_get_countrycode( $sCountryName )
{
    switch ( trim( $sCountryName ) ) 
    {
        case 'CN':
                  $sCountryName = "CA";
                  break;
        case 'CA':
                  $sCountryName = "CA";
                  break;      
        case 'CAN':
                  $sCountryName = "CA";
                  break;
        case 'US':
                  $sCountryName = "US";
                  break;      
        case 'USA':
                  $sCountryName = "US";
                  break;              
    }
    return $sCountryName;
}
   
add_action( 'wp_ajax_odfl_save_warehouse', 'save_odfl_warehouse_ajax' );
add_action( 'wp_ajax_nopriv_odfl_save_warehouse', 'save_odfl_warehouse_ajax' );

/**
 * Save Warehouse Function
 * @global $wpdb
 * @return array
 */
function save_odfl_warehouse_ajax() 
{
    global $wpdb;

    $post_data_arr  = array(
        'city'      => $_POST['origin_city'],
        'state'     => $_POST['origin_state'],
        'zip'       => $_POST['origin_zip'],
        'country'   => $_POST['origin_country'],
        'location'  => $_POST['location']
    );

    $odflValidateData   =  odfl_validating_post_data($post_data_arr);

    if( $odflValidateData["city"] != 'Error' && $odflValidateData["zip"] != 'Error' )
    {
        $get_warehouse  = $wpdb->get_results(
                "SELECT * FROM ".$wpdb->prefix."warehouse WHERE city = '" . $odflValidateData["city"] . "' && state = '" . $odflValidateData["state"] . "' && zip = '" . $odflValidateData["zip"] . "'");

        if (isset($odflValidateData["city"])) 
        {
            $get_warehouse_id  = $_POST['origin_id'];
            $postData          = odfl_filtered_data( $odflValidateData );

            if ($get_warehouse_id && empty($get_warehouse)) 
            {
                $update_qry = $wpdb->update(
                    $wpdb->prefix.'warehouse', $postData, array('id'  => $get_warehouse_id)
                );
            }
            else 
            {
                if (empty($get_warehouse)) 
                {
                    $insert_qry = $wpdb->insert(
                        $wpdb->prefix.'warehouse', $postData
                    );
                }
            }
        }

        $lastid = $wpdb->insert_id;
        if ($lastid == 0){
            $lastid = $get_warehouse_id;
        }

        $warehous_list = array('origin_city' => $postData["city"], 'origin_state' => $postData["state"], 'origin_zip' => $postData["zip"], 'origin_country' => odfl_get_countrycode($postData["country"]), 'insert_qry' => $insert_qry, 'update_qry' => $update_qry, 'id' => $lastid);
        echo json_encode($warehous_list);
        exit;
    }
    else 
    {
        echo "false";
        exit;
    }
}

add_action( 'wp_ajax_odfl_edit_warehouse', 'edit_odfl_warehouse_ajax' );
add_action( 'wp_ajax_nopriv_odfl_edit_warehouse', 'edit_odfl_warehouse_ajax' );

/**
 * Edit Warehouse Function
 * @global $wpdb
 * @return json
 */
function edit_odfl_warehouse_ajax()
{
    global $wpdb;
    $warehous_list = $wpdb->get_results( 
        "SELECT id, city, state, zip, country 
        FROM ".$wpdb->prefix . "warehouse WHERE id=".( isset( $_POST['edit_id']) && intval($_POST['edit_id']) ? $_POST['edit_id'] : '').""
    );
    echo json_encode($warehous_list);
    exit;
}

add_action( 'wp_ajax_odfl_delete_warehouse', 'delete_odfl_warehouse_ajax' );
add_action( 'wp_ajax_nopriv_odfl_delete_warehouse', 'delete_odfl_warehouse_ajax' );

/**
 * Delete Warehouse Function
 * @global $wpdb
 * @return string
 */
function delete_odfl_warehouse_ajax()
{
    global $wpdb;
    $qry = $wpdb->delete( $wpdb->prefix.'warehouse', array( 'id' => ( isset( $_POST['delete_id'] ) && intval( $_POST['delete_id'] ) ? $_POST['delete_id'] : ''), 'location' => 'warehouse' ) );
    echo $qry;
    exit;
}

add_action( 'wp_ajax_odfl_save_dropship', 'save_odfl_dropship_ajax' );
add_action( 'wp_ajax_nopriv_odfl_save_dropship', 'save_odfl_dropship_ajax' );

/**
 * Save Dropship Function
 * @global $wpdb
 * @return array
 */
function save_odfl_dropship_ajax() 
{
    global $wpdb;
    $odfl_input_post_data_arr = array(
        'city'      => $_POST['dropship_city'],
        'state'     => $_POST['dropship_state'],
        'zip'       => $_POST['dropship_zip'],
        'country'   => $_POST['dropship_country'],
        'location'  => $_POST['location'],
        'nickname'  => $_POST['nickname']
    );
    $odfl_validating_data   =  odfl_validating_post_data($odfl_input_post_data_arr);

    if( $odfl_validating_data["city"] != 'Error' && $odfl_validating_data["nickname"] != 'Error' && $odfl_validating_data["zip"] != 'Error' )
    {
        $get_warehouse = $wpdb->get_results(
                "SELECT * FROM ".$wpdb->prefix."warehouse WHERE city = '" . $odfl_validating_data["city"] . "' && state = '" . $odfl_validating_data["state"] . "' && zip = '" . $odfl_validating_data["zip"] . "' && nickname = '" . $odfl_validating_data["nickname"] . "'");

        if (isset($odfl_validating_data["city"])) 
        {
            $get_dropship_id = $_POST['dropship_id'];
            $postData        = odfl_filtered_data( $odfl_validating_data );

            if ($get_dropship_id != '' && empty($get_warehouse)) 
            {
                $update_qry = $wpdb->update(
                    $wpdb->prefix.'warehouse', $postData, array('id' => $get_dropship_id)
                );
            }
            else 
            {
                if (empty($get_warehouse)) 
                {
                    $insert_qry = $wpdb->insert(
                        $wpdb->prefix.'warehouse', $postData
                    );
                }
            }
        }
        $lastid = $wpdb->insert_id;
        if ($lastid == 0){
            $lastid = $get_dropship_id;
        }

        $warehous_list = array('nickname' => $postData["nickname"], 'origin_city' => $postData["city"], 'origin_state' => $postData["state"], 'origin_zip' => $postData["zip"], 'origin_country' => odfl_get_countrycode($postData["country"]), 'insert_qry' => $insert_qry, 'update_qry' => $update_qry, 'id' => $lastid);
        echo json_encode($warehous_list);
        exit;
    }
    else 
    {
        echo "false";
        exit;
    }  
}

add_action( 'wp_ajax_odfl_edit_dropship', 'edit_odfl_dropship_ajax' );
add_action( 'wp_ajax_nopriv_odfl_edit_dropship', 'edit_odfl_dropship_ajax' );

/**
 * Edit Dropship Function
 * @global $wpdb
 * @return json
 */
function edit_odfl_dropship_ajax()
{
    global $wpdb;
    $warehous_list = $wpdb->get_results( 
      "SELECT id, city, state, zip, country, nickname 
      FROM ".$wpdb->prefix . "warehouse WHERE id=".( isset( $_POST['dropship_edit_id'] ) && intval( $_POST['dropship_edit_id'] ) ? $_POST['dropship_edit_id'] : '').""
    );
    echo json_encode($warehous_list);
    exit;
}

add_action( 'wp_ajax_odfl_delete_dropship', 'delete_odfl_dropship_ajax' );
add_action( 'wp_ajax_nopriv_odfl_delete_dropship', 'delete_odfl_dropship_ajax' );

/**
 * Delete Dropship Function
 * @global $wpdb
 * @return string
 */
function delete_odfl_dropship_ajax()
{
    global $wpdb; 
    $dropship_id            = ( isset($_POST['dropship_delete_id']) && intval( $_POST['dropship_delete_id'] ) ) ? $_POST['dropship_delete_id'] : "";
    $get_dropship_id        = array($dropship_id);
    $dropship_location_val  = array_map( 'intval', $get_dropship_id );
    $ser                    = maybe_serialize($dropship_location_val);
    $get_post_id            = $wpdb->get_results("SELECT group_concat(post_id) as post_ids_list FROM `".$wpdb->prefix ."postmeta` WHERE `meta_key` = '_dropship_location' AND (`meta_value` LIKE '%".$ser."%' OR `meta_value` = '".$dropship_id."')");
    $post_id                = reset($get_post_id)->post_ids_list;

    if(isset($post_id)){
        $wpdb->query("UPDATE `".$wpdb->prefix ."postmeta` SET `meta_value` = '' WHERE `meta_key` IN('_enable_dropship','_dropship_location')  AND `post_id` IN ($post_id)");
    }

    $qry = $wpdb->delete($wpdb->prefix."warehouse", array('id' => $dropship_id, 'location' => 'dropship'));
    echo $qry;
    exit;
}
