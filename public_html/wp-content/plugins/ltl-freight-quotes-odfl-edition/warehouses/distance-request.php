<?php
/**
 * Get Distance From Zipcodes 
 * @package     Woocommerce ODFL Edition
 * @author      <https://eniture.com/>
 * @copyright   Copyright (c) 2017, Eniture
 */
if ( ! defined( 'ABSPATH' ) ) {
    exit; 
}

/**
 * Getting Distance From Zipcodes Class
 */
class ODFL_Get_Distance 
{
    /**
     * Get distance curl request
     * @param $map_address
     * @param $accessLevel
     * @return json
     */
    function odfl_address($map_address, $accessLevel) 
    {
        $post = array(
            'acessLevel'        => $accessLevel,
            'address'           => $map_address,
            'eniureLicenceKey'  => get_option('wc_settings_odfl_plugin_licence_key'),
            'ServerName'        => $_SERVER['SERVER_NAME']
        );

        $odfl_curl_obj = new ODFL_Curl_Request();
        $output        = $odfl_curl_obj->odfl_get_curl_response('http://eniture.com/ws/addon/google-location.php', $post);
        return $output;
    }
}
