<?php
/**
 * Get Shipping Package Class
 * @package     Woocommerce ODFL Edition
 * @author      <https://eniture.com/>
 * @copyright   Copyright (c) 2017, Eniture
 */
if ( ! defined( 'ABSPATH' ) ) {
    exit; 
}

/**
 * Get Shipping Package Class
 */

    class ODFL_Shipping_Get_Package 
    {
        /**
         * LTL has shipment
         * @var int
         */
        public $hasLTLShipment  = 0;
        /**
         * Display error message
         * @var varchar
         */
        public $errors          = array();

        /**
         * Grouping For Shipments
         * @param $package
         * @param $odfl_res_inst
         * @param $odfl_freight_zipcode
         * @return Shipment Grouped Array
         */
        function group_odfl_shipment( $package, $odfl_res_inst, $odfl_freight_zipcode ) 
        {   
            global $wpdb;
            $weight              = 0;
            $dimensions          = 0;
            $odfl_freight_class  = "";
            $odfl_enable         = false;
            $odfl_woo_obj        = new ODFL_Woo_Update_Changes();              
            $odfl_zipcode        = $odfl_woo_obj->odfl_postcode();

            if(empty($odfl_freight_zipcode)){
                return array();
            }
            foreach ( $package['contents'] as $item_id => $values ) 
            {
                $_product       = $values['data'];
                $height         = wc_get_dimension( $_product->get_height(), 'in' );
                $width          = wc_get_dimension( $_product->get_width(), 'in' );
                $length         = wc_get_dimension( $_product->get_length(), 'in' );
                $product_weight = wc_get_weight( $_product->get_weight(), 'lbs' );
                $weight         = ( $values['quantity'] == 1 ) ? $product_weight : $product_weight * $values['quantity'];
                $dimensions     = (($length * $values['quantity']) * $width * $height);   
                
                if ($_product->get_shipping_class() == 'ltl_freight') {
                    $ltl_freight_class = $_product->get_shipping_class();
                }

                $locationId  = 0;
                (isset($values['variation_id']) && $values['variation_id'] > 0 ) ? $post_id = $values['variation_id'] : $post_id = $_product->get_id();
                $locations_list                       = $this->odfl_get_locations_list($post_id);
                $origin_address                       = $odfl_res_inst->odfl_multi_warehouse($locations_list, $odfl_zipcode);
                $getFreightClassAndHazardous          = $this->odfl_get_freight_class_hazardous($_product, $values['variation_id'], $values['product_id']);
                ($getFreightClassAndHazardous["freightClass_ltl_gross"] == 'Null') ? $getFreightClassAndHazardous["freightClass_ltl_gross"] = "" : "";
                
                $locationId                           = $origin_address['locationId'];
                $odfl_package[$locationId]['origin']  = $origin_address;
                
                if ( !$_product->is_virtual() ) {
                    $odfl_package[$locationId]['items'][] = array(
                        'productId'      => $_product->get_id(),
                        'productName'    => $_product->get_title(),
                        'productQty'     => $values['quantity'],
                        'productPrice'   => $_product->get_price(),
                        'productWeight'  => $product_weight,
                        'productLength'  => $length,
                        'productWidth'   => $width,
                        'productHeight'  => $height,
                        'productClass'   => $getFreightClassAndHazardous["freightClass_ltl_gross"]
                    );
                }
                $odfl_enable   = $this->get_odfl_enable( $_product );
                $exceedWeight  = get_option('en_plugins_return_LTL_quotes');
                $odfl_package[$locationId]['shipment_weight']    = isset($odfl_package[$locationId]['shipment_weight']) ? $odfl_package[$locationId]['shipment_weight']+$weight : $weight;
                $odfl_package[$locationId]['hazardousMaterial']  = isset($odfl_package[$locationId]['hazardousMaterial']) && $odfl_package[$locationId]['hazardousMaterial'] == 'yes' ? $odfl_package[$locationId]['hazardousMaterial'] : $getFreightClassAndHazardous["hazardous_material"];

                $smallPluginExist  = 0;
                $calledMethod      = array();
                $eniturePluigns    = json_decode(get_option('EN_Plugins'));

                if( !empty($eniturePluigns) ){
                    foreach ($eniturePluigns as $enIndex => $enPlugin) {
                        $freightSmallClassName = 'WC_' . $enPlugin;
                        if (!in_array($freightSmallClassName, $calledMethod)) {
                            if (class_exists($freightSmallClassName)) {
                                $smallPluginExist = 1;
                            }
                            $calledMethod[] = $freightSmallClassName;
                        }
                    }
                }
                if ($odfl_enable == true || ($odfl_package[$locationId]['shipment_weight'] > 150 && $exceedWeight == 'yes') ) {                           
                    $odfl_package[$locationId]['odfl']  = 1;
                    $this->hasLTLShipment               = 1;
                } elseif (isset($odfl_package[$locationId]['odfl'])) {
                    $odfl_package[$locationId]['odfl']  = 1;
                    $this->hasLTLShipment               = 1;
                } elseif($smallPluginExist == 1) {
                    $odfl_package[$locationId]['small']  = 1;
                } 
            }
            return $odfl_package;
        }

        /**
         * Check enable_dropship and get Locations list
         * @global $wpdb
         * @param $post_id
         * @return array
         */
        function odfl_get_locations_list( $post_id ) 
        {           
            global $wpdb;
            $enable_dropship = get_post_meta($post_id, '_enable_dropship', true);

            if ($enable_dropship == 'yes') 
            {
                $get_loc = get_post_meta($post_id, '_dropship_location', true);

                if($get_loc =='')
                    return array('error'=>'dp location not found!');


                $get_loc         = ($get_loc !== '')?maybe_unserialize($get_loc):$get_loc;
                $get_loc         = is_array($get_loc)? implode(" ', '", $get_loc):$get_loc;
                
                $locations_list  = $wpdb->get_results(
                    "SELECT id, city, state, zip, country, location, nickname FROM ".$wpdb->prefix."warehouse WHERE id IN ('". $get_loc."')"
                );
            } 
            else 
            {
                $locations_list = $wpdb->get_results(
                    "SELECT id, city, state, zip, country, location FROM ".$wpdb->prefix."warehouse WHERE location = 'warehouse'"
                );
            }
            
            return $locations_list;
        }

        /**
         * Get Freight Class and Hazardous Material Checkbox
         * @param $_product
         * @param $variation_id
         * @param $product_id
         * @return array
         */
        function odfl_get_freight_class_hazardous( $_product, $variation_id, $product_id ) 
        {
            if ($_product->get_type() == 'variation') 
            {
                $hazardous_material    = get_post_meta($variation_id, '_hazardousmaterials', true);
                $variation_class       = get_post_meta($variation_id, '_ltl_freight_variation', true);

                if($variation_class == 'get_parent')
                {
                    $variation_class         = get_post_meta($product_id, '_ltl_freight', true);
                    $freightClass_ltl_gross  = $variation_class;
                }
                else
                { 
                    if ($variation_class > 0) {
                        $freightClass_ltl_gross = get_post_meta($variation_id, '_ltl_freight_variation', true);
                    } 
                    else {
                        $freightClass_ltl_gross = get_post_meta($_product->get_id(), '_ltl_freight', true);
                    }
                }

            } 
            else 
            {
                $hazardous_material      = get_post_meta($_product->get_id(), '_hazardousmaterials', true);
                $freightClass_ltl_gross  = get_post_meta($_product->get_id(), '_ltl_freight', true);
            }

            $aDataArr = array(
                'freightClass_ltl_gross' => $freightClass_ltl_gross,
                'hazardous_material'     => $hazardous_material
            );

            return $aDataArr;
        }


        /**
         * Get ODFL Enable or not 
         * @param $_product
         * @return type
         */
        function get_odfl_enable( $_product ) 
        {
            if( $_product->get_type() == 'variation' )
            {
                $ship_class_id = $_product->get_shipping_class_id();
                if($ship_class_id == 0)
                {
                    $parent_data          = $_product->get_parent_data();
                    $get_parent_term      = get_term_by( 'id', $parent_data['shipping_class_id'], 'product_shipping_class' );
                    $get_shipping_result  = ( isset($get_parent_term->slug) ) ? $get_parent_term->slug : '';

                }
                else{
                    $get_shipping_result  = $_product->get_shipping_class();
                }

                $odfl_enable  = ($get_shipping_result && $get_shipping_result == 'ltl_freight') ?  true : false;
            }
            else {
                $get_shipping_result   = $_product->get_shipping_class();
                $odfl_enable           = ($get_shipping_result  == 'ltl_freight') ?  true : false;
            }  

            return $odfl_enable;
        }

        /**
         * Grouping For Shipment Quotes
         * @param $quotes
         * @param $handlng_fee
         * @return array
         */   
        function odfl_grouped_quotes( $quotes, $handlng_fee ) 
        {
            $totalPrice   = 0;
            $grandTotal   = 0;
            $freight      = array();

            if(count($quotes) > 0 && !empty($quotes) ) 
            {  
                foreach ( $quotes as $multiValues )
                { 
                    if( isset( $multiValues['cost'] ) && !empty( $multiValues['cost'] ) ) 
                    {
                        $totalPrice   = $multiValues['cost'];         
                        $grandTotal  += ( floatval( $handlng_fee ) && !empty($handlng_fee) ) ? $this->odfl_parse_handeling_fee($handlng_fee, $totalPrice) : $totalPrice;
                    }
                    else
                    {
                        $this->errors = 'no quotes return'; 
                        continue;
                    }                 
                }
            }
            
            $freight     = array(
                'totals'  => $grandTotal
            );       
            return $freight;
        }  

        /**
         * Grouping For Small Quotes
         * @param $smallQuotes
         * @return int
         */
        function odfl_get_small_package_cost($smallQuotes) 
        {
            $result      = array();
            $minCostArr  = array();

            if (isset($smallQuotes) && count($smallQuotes) > 0) 
            {
                foreach ($smallQuotes as $smQuotes) 
                { 
                    $CostArr = array();
                    if(!isset($smQuotes['error']))
                    {
                        foreach ($smQuotes as $smQuote) 
                        { 
                            $CostArr[]        = $smQuote['cost'];
                            $result['error']  = false;
                        }
                        $minCostArr[]  = ( count($CostArr) > 0 ) ? min($CostArr) : "";
                    }
                    else{
                        $result['error']  = !isset($result['error'])?true:$result['error'];
                    }
                }
                $result['price']  = (isset($minCostArr) && count($minCostArr) > 0 )? min($minCostArr): "";
            }
            else
            {
                $result['error']  = false;
                $result['price']  = 0;
            }
            return $result;
        }

        /**
         * Calculate Handeling Fee
         * @param $handlng_fee
         * @param $cost
         * @return double
         */
        function odfl_parse_handeling_fee( $handlng_fee, $cost ) 
        {
            $pos = strpos( $handlng_fee, '%' );
            if ($pos > 0) 
            {
                $rest        = substr( $handlng_fee, $pos );
                $exp         = explode( $rest, $handlng_fee );
                $get         = $exp[0];
                $percnt      = $get / 100 * $cost;
                $grandTotal  = $cost + $percnt;
            }
            else{
                $grandTotal  = $cost + $handlng_fee;
            }
            return $grandTotal;
        }
    }