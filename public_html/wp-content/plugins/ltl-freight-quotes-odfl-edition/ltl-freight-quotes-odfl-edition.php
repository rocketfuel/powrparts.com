<?php
/**
Plugin Name: LTL Freight Quotes - ODFL Edition
Plugin URI: https://eniture.com/products/
Description: Dynamically retrieves your negotiated shipping rates from ODFL Freight and displays the results in the WooCommerce shopping cart.
Version: 1.1.0
Author: Eniture Technology
Author URI: http://eniture.com/
Text Domain: eniture-technology
License: GPL version 2 or later - http://www.eniture.com/
*/

if ( ! defined( 'ABSPATH' ) ){ 
    exit; // Exit if accessed directly.
}

if ( ! function_exists( 'is_plugin_active' ) ){		
    require_once( ABSPATH . 'wp-admin/includes/plugin.php' );
}


if ( ! is_plugin_active( 'woocommerce/woocommerce.php' ) ) {
    add_action( 'admin_notices', 'odfl_wc_avaibility_error' );
}
/**
 * Check WooCommerce installlation 
*/
function odfl_wc_avaibility_error() 
{
    $class    = "error";
    $message  = "LTL Freight Quotes - ODFL Edition is enabled, but not effective. It requires WooCommerce in order to work, please <a target='_blank' href='https://wordpress.org/plugins/woocommerce/installation/'>Install</a> WooCommerce Plugin. Reactivate LTL Freight Quotes - ODFL Edition plugin to create LTL shipping class.";
    echo"<div class=\"$class\"> <p>$message</p></div>";
}


add_action( 'admin_init' , 'odfl_check_wc_version' );

/**
* Check WooCommerce version compatibility
*/
function odfl_check_wc_version() 
{
    $woo_version  = odfl_wc_version_number();
    $version      = '2.6';
    if (!version_compare($woo_version, $version, ">=")) {
        add_action( 'admin_notices' , 'wc_version_incompatibility_odfl' );
    }
}

/**
 * WooCommerce version incompatibility check
 */
function wc_version_incompatibility_odfl() 
{
?>
    <div class="notice notice-error">
        <p>
<?php 
            _e( 'LTL Freight Quotes - ODFL Edition plugin requires WooCommerce version 2.6 or higher to work. Functionality may not work properly.', 'wwe-woo-version-failure' ); 
?>
        </p>
    </div>
<?php
}

/**
 * ODFL version number
 * @return WooCommerce version 
*/        
function odfl_wc_version_number() 
{
    $plugin_folder  = get_plugins('/' . 'woocommerce');
    $plugin_file    = 'woocommerce.php';

    if ( isset( $plugin_folder[$plugin_file]['Version'] ) ) 
        return $plugin_folder[$plugin_file]['Version'];
    else 
        return NULL;

}

if ( in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) 
{
    if( !isset( $_SESSION ) ){ 
        session_start(); 
    }

    add_action( 'admin_enqueue_scripts', 'odfl_admin_script' );
    /**
     * Load scripts for ODFL
     */
    function odfl_admin_script() 
    {  
        wp_enqueue_script( 'odfl_script', plugin_dir_url( __FILE__ ) . '/js/odfl-warehouse.js', array(), '1.0.0' );
        wp_localize_script('odfl_script', 'script', array(
            'pluginsUrl' => plugins_url(),
        ));
        wp_register_style( 'odfl-style', plugin_dir_url( __FILE__ ) . '/css/odfl-style.css', false, '1.0.0' );
        wp_enqueue_style( 'odfl-style' );
    }

/**
 * Inlude Plugin Files
 */  

    require_once ( 'js/odfl-js.php' );
    require_once ( 'odfl-test-connection.php' );
    require_once ( 'odfl-shipping-class.php' );
    require_once ( 'db/odfl-db.php' );
    require_once ( 'odfl-admin-filter.php' );
    require_once ( 'template/odfl-product-detail.php' );
    require_once ( 'warehouses/save-warehouse.php' );
    require_once ( 'warehouses/distance-request.php' );
    require_once ( 'odfl-group-package.php' );
    require_once ( 'odfl-carrier-service.php' );
    require_once ( 'odfl-wc-update-change.php' );
    require_once ( 'template/connection-settings.php' );
    require_once ( 'template/quote-settings.php' );
    require_once ( 'odfl-curl-class.php' );
    require_once ( ABSPATH . 'wp-admin/includes/upgrade.php' );

/**
 * ODFL Activation Hook
 */
    register_activation_hook( __FILE__, 'create_odfl_ltl_freight_class' );
    register_activation_hook( __FILE__, 'create_odfl_wh_db' );
    register_activation_hook( __FILE__, 'create_odfl_option' );

/*
 * ODFL Action And Filters
 */

    add_action( 'woocommerce_shipping_init', 'odfl_logistics_init' );
    add_action( 'woocommerce_process_product_meta', 'odfl_woo_add_custom_general_fields_save' );
    add_action( 'woocommerce_save_product_variation', 'odfl_save_variable_fields', 10, 2 );
    add_filter( 'woocommerce_shipping_methods', 'add_odfl_logistics' );
    add_filter( 'woocommerce_get_settings_pages', 'odfl_shipping_sections' );
    add_filter( 'woocommerce_package_rates', 'odfl_hide_shipping' );  
    add_filter( 'woocommerce_shipping_calculator_enable_city', '__return_true' );
    add_filter('plugin_action_links', 'odfl_logistics_add_action_plugin', 10, 5);
    
    /**
     * ODFL action links
     * @staticvar $plugin
     * @param $actions
     * @param $plugin_file
     * @return arrray
     */
    function odfl_logistics_add_action_plugin($actions, $plugin_file) 
    {
        static $plugin;
        if (!isset($plugin))
            $plugin = plugin_basename(__FILE__);
        if ($plugin == $plugin_file) 
        {
            $settings    = array('settings'  => '<a href="admin.php?page=wc-settings&tab=odfl_quotes">' . __('Settings', 'General') . '</a>');
            $site_link   = array('support'   => '<a href="https://eniture.com/support/" target="_blank">Support</a>');
            $actions     = array_merge($settings, $actions);
            $actions     = array_merge($site_link, $actions);
        }
        return $actions;
    } 

    add_filter( 'woocommerce_cart_no_shipping_available_html', 'odfl_cart_html_message' );
    /**
    * No Quotes Cart Message
    * @return  string 
    */
    function odfl_cart_html_message(){
        echo"<div><p>There are no shipping methods available. Please double check your address, or contact us if you need any help.</p></div>";
    }
}
