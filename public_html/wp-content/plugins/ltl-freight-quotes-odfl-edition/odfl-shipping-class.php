<?php
/**
 * Get Shipping Package Class
 * @package     Woocommerce ODFL Edition
 * @author      <https://eniture.com/>
 * @copyright   Copyright (c) 2017, Eniture
 */
if ( ! defined( 'ABSPATH' ) ) {
    exit; 
}


/**
 * ODFL Initialize
 * @return Shipping carriers
 */
    function odfl_logistics_init() 
    {
        if ( ! class_exists( 'ODFL_Freight_Shipping_Class' ) ) 
        {
            /**
             * ODFL Shipping Calculation Class
            */
                     
            class ODFL_Freight_Shipping_Class extends WC_Shipping_Method 
            {
                /**
                 * Woocommerce Shipping Field Attributes
                 * @param $instance_id
                */
                public function __construct($instance_id = 0) 
                {        
                    $this->id                  = 'odfl'; 
                    $this->instance_id         = absint($instance_id);
                    $this->method_title        = __( 'ODFL Freight' );
                    $this->method_description  = __('Shipping rates from ODFL Freight.');
                    $this->supports            = array(
                                                    'shipping-zones',
                                                    'instance-settings',
                                                    'instance-settings-modal',
                                                );
                    $this->enabled             = "yes";
                    $this->title               = 'LTL Freight Quotes - ODFL Edition';
                    $this->init();
                    add_action('woocommerce_checkout_update_order_review', array($this, 'calculate_shipping'));
                }

                /**
                 * Init
                 */
                function init() 
                {
                    $this->init_form_fields();
                    $this->init_settings(); 
                    add_action( 'woocommerce_update_options_shipping_' . $this->id, array( $this, 'process_admin_options' ) );
                }

                /**
                 * Enable Woocommerce Shipping For ODFL
                */
                function init_form_fields() 
                {
                    $this->instance_form_fields = array(
                        'enabled'  => array(
                            'title'    => __('Enable / Disable', 'odfl'),
                            'type'     => 'checkbox',
                            'label'    => __('Enable This Shipping Service', 'odfl'),
                            'default'  => 'no',
                            'id'       => 'odfl_enable_disable_shipping'
                        )
                    );
                }

                /**
                 * Calculate Shipping Rates For ODFL
                 * @param string $package
                 * @return boolean|string
                 */
                public function calculate_shipping( $package=array() ) 
                {
                    $coupn = WC()->cart->get_coupons();
                    if(isset($coupn) && !empty($coupn)){
                        $freeShipping = $this->odfl_freight_free_shipping($coupn);
                        if($freeShipping == 'y') return FALSE;
                    }

                    $odfl_woo_obj      = new ODFL_Woo_Update_Changes();
                    $freight_zipcode   = "";
                    (strlen(WC()->customer->get_shipping_postcode()) > 0) ? $freight_zipcode = WC()->customer->get_shipping_postcode() : $freight_zipcode = $odfl_woo_obj->odfl_postcode();
                    $obj            = new ODFL_Shipping_Get_Package();
                    $odfl_res_inst  = new ODFL_Get_Shipping_Quotes();
                    $odfl_package   = $obj->group_odfl_shipment($package, $odfl_res_inst, $freight_zipcode);                   
		    $handlng_fee    = get_option('odfl_handling_fee');
                    $quotes         = array();
                    $rate           = array();

                    if (isset($odfl_package['error'])) {
                        return 'error';
                    }

                    $eniturePluigns    = json_decode(get_option('EN_Plugins'));
                    $calledMethod      = array();
                    $smallPluginExist  = 0;
                    $smallQuotes       = array();

                    if( isset($odfl_package) && !empty($odfl_package) )
                    {
                        foreach ( $odfl_package as $locId => $sPackage ) 
                        {                     
                            if( array_key_exists( 'odfl', $sPackage ) ) 
                            {    
                                $web_service_arr   = $odfl_res_inst->odfl_shipping_array( $sPackage );  
                                $quotes[]          = $odfl_res_inst->odfl_get_web_quotes( $web_service_arr );
                                continue;
                             }

                            if(array_key_exists('small', $sPackage))
                            {  
                                foreach ($eniturePluigns as $enIndex => $enPlugin) 
                                {
                                    $freightSmallClassName = 'WC_' . $enPlugin;

                                    if (!in_array($freightSmallClassName, $calledMethod)) 
                                    {
                                        if (class_exists($freightSmallClassName)) 
                                        {
                                            $smallPluginExist     = 1;
                                            $SmallClassNameObj    = new $freightSmallClassName();
                                            $package['itemType']  = 'ltl';                                            
                                            $smallQuotesResponse  = $SmallClassNameObj->calculate_shipping($package);
                                            $smallQuotes[]        = $smallQuotesResponse;
                                        }
                                        $calledMethod[] = $freightSmallClassName;
                                    }
                                }
                            }
                            else {
                                return FALSE;
                            }
                        }   
                    }    

                    if ( in_array("error", $quotes) ) {
                        return 'error';
                    }

                    $smpkgCost     = 0;
                    $smallMinRate  = $obj->odfl_get_small_package_cost($smallQuotes);

                    if(isset($smallMinRate['error']) && $smallMinRate['error']== true){
                        return FALSE;
                    }
                    $smpkgCost = $smallMinRate['price'];

                    if ( count( $quotes ) > 1  || $smpkgCost > 0 )
                    {   
                        foreach ( $quotes as $multi_values ) 
                        {
                            $freight = $obj->odfl_grouped_quotes( $quotes, $handlng_fee );

                            if( isset( $freight ) && $freight != "" && $freight['totals'] > 0 )
                            {
                                $rate = array(
                                    'id'     => $this->id,
                                    'label'  => 'Freight',
                                    'cost'   => $freight['totals']+$smpkgCost,
                                );
                            }
                        }

                        if( isset( $rate ) ){ 
                            $this->add_rate( $rate ); 
                        }
                    }
                    else
                    {                        
                        foreach ($quotes as $value) 
                        {
                            if(isset($value) && !empty($value['cost']))
                            {
                                $cost           = $value['cost'];
                                $estimtdDays    = $value['transit_time'];  
                                $label          = get_option('odfl_label_as');
                                (empty($label)) ? $label = 'Freight' : '';

                                if (get_option('odfl_delivey_estimate') == 'yes'){
                                    $label = $label.' ( Estimated transit time of '.$estimtdDays.' business days. )';
                                }

                                $grandTotal = ( $handlng_fee != '' ) ? $obj->odfl_parse_handeling_fee($handlng_fee, $cost) : $cost;

                                if($grandTotal > 0)
                                {
                                    $rate = array(
                                        'id'     => $this->id,
                                        'label'  => $label,
                                        'cost'   => $grandTotal,
                                    );
                                }
                            }
                        }

                        if( isset( $rate ) ){ 
                            $this->add_rate( $rate ); 
                        }
                    }                   
                }

                /**
                 * ODFL Coupon for free shipping
                 * @param $coupon
                 * @return string
                 */
                function odfl_freight_free_shipping($coupon) {
                    foreach ($coupon as $key => $value) {
                        if($value->get_free_shipping() == 1){
                            $rates = array(
                                'id' => 'free',
                                'label' => 'Free Shipping',
                                'cost'  => 0
                            );
                            $this->add_rate($rates);
                            return 'y';
                        }
                    }
                    return 'n';
                }
            }
        }
    }
