<?php
/**
 * Test Connection | ODFL test connection
 * @package     Woocommerce ODFL Edition
 * @author      <https://eniture.com/>
 * @copyright   Copyright (c) 2017, Eniture
 */
if ( ! defined( 'ABSPATH' ) ) {
    exit; 
}
add_action('wp_ajax_nopriv_odfl_action', 'odfl_test_submit');
add_action('wp_ajax_odfl_action', 'odfl_test_submit');

/**
 * ODFL test connection ajax request 
 * @return array
 */
function odfl_test_submit() 
{ 
    $sRequestData = array(
        'licence_key'          => ( isset( $_POST['odfl_plugin_license'] ) ) ? sanitize_text_field($_POST['odfl_plugin_license']) : "",
        'sever_name'           => $_SERVER['SERVER_NAME'],
        'carrierName'          => 'odfl4me',
        'carrier_mode'         => 'test',
        'odflUserName'         => ( isset( $_POST['odfl_username'] ) )  ? sanitize_text_field($_POST['odfl_username'])  : "",
        'odflPassword'         => ( isset( $_POST['odfl_password'] ) )  ? sanitize_text_field($_POST['odfl_password'])  : "",
        'odflCustomerAccount'  => ( isset( $_POST['odfl_accountno'] ) ) ? sanitize_text_field($_POST['odfl_accountno']) : "",
    );

    $odfl_curl_obj  = new ODFL_Curl_Request();
    $sResponseData  = $odfl_curl_obj->odfl_get_curl_response('http://eniture.com/ws/index.php', http_build_query($sRequestData));
    $sResponseData  = json_decode($sResponseData);

    if( isset( $sResponseData->soapenvBody->ns2getLTLRateEstimateResponse->return->success ) && $sResponseData->soapenvBody->ns2getLTLRateEstimateResponse->return->success === "true" ) 
    {
        $sResult = array('message' => "success");
    } 
    elseif ( isset( $sResponseData->error ) || $sResponseData->soapenvBody->ns2getLTLRateEstimateResponse->return->success === "false" ) 
    {
        $sResult = ( isset( $sResponseData->error ) && !empty( $sResponseData->error ) ) ? $sResponseData->error : str_replace(Odfl4me, "" , $sResponseData->soapenvBody->ns2getLTLRateEstimateResponse->return->errorMessages);
        $sResult = array('message' => $sResult);
    }
    else
    {
       $sResult = array('message' => "failure");
    }    

    echo json_encode($sResult);
    exit();
}
