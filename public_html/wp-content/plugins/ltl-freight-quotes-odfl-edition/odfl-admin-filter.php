<?php
/**
 * Admin Settings | all admin settings defined
 * @package     Woocommerce ODFL Edition
 * @author      <https://eniture.com/>
 * @copyright   Copyright (c) 2017, Eniture
 */
if ( ! defined( 'ABSPATH' ) ) {
    exit; 
}

/**
 * Add ODFL Method In WOO Method List
 * @param $methods
 * @return string
 */
function add_odfl_logistics( $methods ) 
{
    $methods['odfl'] = 'ODFL_Freight_Shipping_Class';
    return $methods;
}

/**
 * Add Woo Setting Tab For ODFL
 * @param $settings
 * @return string
 */
function odfl_shipping_sections( $settings ) 
{
    include( 'odfl-tab-class.php' );
    return $settings;
}

/**
 * Hide Other Shipping Options
 * @param $available_methods
 * @return array
 */
function odfl_hide_shipping( $available_methods ) 
{
    if ( get_option( 'odfl_allow_other_plugins' ) == 'no' ) {
        if ( count( $available_methods ) > 0 ) {
            $odfl_rates         = array();
            $plugins_array      = array();
            $eniture_plugins    = get_option('EN_Plugins');
            if( $eniture_plugins ){ 
                $plugins_array = json_decode( $eniture_plugins );
            }
            foreach ( $available_methods as $index => $method ){
                if ( !($method->method_id == 'speedship' || $method->method_id == 'ltl_shipping_method' || in_array( $method->method_id, $plugins_array )) ) {
                    unset($available_methods[$index]);
                }
            }
        }
    } 
    return $available_methods;
}

  
/**
 * Filter For CSV Export
 */
add_filter('woocommerce_product_export_meta_value', 'odfl_ltl_dropship_location_csv', '99', '4');

/**
 * Change dropship location value
 * @param $metaValue
 * @param $meta
 * @param $product
 * @param $row
 * @return string
 */
function odfl_ltl_dropship_location_csv( $metaValue, $meta, $product, $row ) {

    $needle = "a:";
    if (strpos($metaValue, $needle) !== false) {
        $dsLocationIdArr = maybe_unserialize($metaValue);
        foreach ($dsLocationIdArr as $key => $dsLocationId) {
            $metaValue = odfl_ltl_get_dropship_address($dsLocationId);

        }

    }
    return $metaValue;
}
/**
     * Drop ship address
     * @global $wpdb
     * @param $dsLocationId
     * @return string
     */
function odfl_ltl_get_dropship_address($dsLocationId) {
    global $wpdb;
    $location = "";

    if(!empty($dsLocationId) && $dsLocationId != 0){
        $dropship = reset($wpdb->get_results(
                        "SELECT nickname, city, state, zip, country 
                  FROM ".$wpdb->prefix . "warehouse WHERE id=$dsLocationId"
                ));
        $location = $dropship->nickname.','.$dropship->zip.','.$dropship->city.','.$dropship->state.','.$dropship->country;
    }
    return $location;
}

 /**
     * Filter For CSV Import
     */
add_filter('woocommerce_product_importer_parsed_data', 'odfl_ltl_import_dropship_location_csv','99','2');

/**
 * Import drop ship location CSV
 * @param $data
 * @param $this
 * @return array
 */
function odfl_ltl_import_dropship_location_csv($data, $this) {

    foreach ($data['meta_data'] as $key => $metaData) {
        if($metaData['key'] == '_dropship_location'){
            $metaValue = explode(',', $metaData['value']);
            if(count($metaValue) > 1){
                $location = odfl_ltl_serialize_dropship($metaValue);
            }else{
                $location = $metaValue;
            }

            $data['meta_data'][$key]['value'] = $location;
        }
    }
    return $data;
}
 /**
     * Serialize drop ship
     * @global $wpdb
     * @param $metaValue
     * @return string
     */
function odfl_ltl_serialize_dropship($metaValue) {
    global $wpdb;
    $dropship = (array)reset($wpdb->get_results(
                    "SELECT id
                    FROM ".$wpdb->prefix . "warehouse WHERE nickname='$metaValue[0]' AND zip='$metaValue[1]' AND city='$metaValue[2]' AND state='$metaValue[3]' AND country='$metaValue[4]'"
            ));

    $dropship = array_map( 'intval', $dropship );

    if(empty($dropship['id'])){
        $data = odfl_ltl_csv_import_dropship_data( $metaValue );
        $wpdb->insert(
            $wpdb->prefix.'warehouse', $data
        );

        $dsId[] = $wpdb->insert_id;
    }else{
        $dsId[] = $dropship['id'];
    }

    $sereializedData = maybe_serialize($dsId);
    return $sereializedData;
}

/**
 * Filtered Data Array
 * @param $metaValue
 * @return array
 */
function odfl_ltl_csv_import_dropship_data( $metaValue ) {
    return array(
        'city'      => $metaValue[2],
        'state'     => $metaValue[3],
        'zip'       => $metaValue[1],
        'country'   => $metaValue[4],
        'location'  => 'dropship',                     
        'nickname'  => ( isset( $metaValue[0] ) ) ? $metaValue[0] : "",
    );
}