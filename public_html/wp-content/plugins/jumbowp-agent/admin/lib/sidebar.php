<?php

namespace rtCamp\WP\Nginx {

	function default_admin_sidebar() {

		$purge_url = add_query_arg( array( 'nginx_helper_action' => 'purge', 'nginx_helper_urls' => 'all' ) );
		$nonced_url = wp_nonce_url( $purge_url, 'nginx_helper-purge_all' );
		echo '<form id="purgeall" action="" method="post" class="clearfix">';
		echo '<a href="'.$nonced_url.'" class="button-primary">Purge Entire Cache</a>';
		echo '</form>';

		echo '
		<div class="postbox" id="social">
			<h3 class="hndle">
				<span>Getting Social is Good</span>
			</h3>
			<div style="text-align:center;" class="inside">
				<a class="nginx-helper-twitter" title="Follow us on Twitter" target="_blank" href="https://twitter.com/jumbowp/"></a>
				<a class="nginx-helper-rss" title="Subscribe to our feeds" target="_blank" href="http://feeds.jumbowp.com/"></a>
			</div>
		</div>
		';
	}
}
