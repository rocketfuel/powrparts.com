jQuery( document ).ready( function() {

	// Handle Purge all
	jQuery("form#purgeall a").click( function(e) {
		if ( confirm( "Purging entire cache is not recommended. Would you like to continue ?" ) == true ) {
			// continue submitting form
		} else {
			e.preventDefault();
		}
	});

	/**
	 * Show OR Hide options on option checkbox
	 * @param {type} selector Selector of Checkbox and PostBox
	 */
	function nginx_show_option(selector) {
		jQuery('#' + selector).on( 'change', function() {
			if (jQuery(this).is(':checked')) {
				jQuery('.' + selector).show();
			} else {
				jQuery('.' + selector).hide();
			}
		});
	}
	/* Function call with parameter */
	nginx_show_option('cache_method_fastcgi');
	nginx_show_option('enable_map');
	nginx_show_option('enable_log');
	nginx_show_option('enable_purge');
});
